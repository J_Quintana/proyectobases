USE GRSECURE;

insert into departamento (id, sueldo, descripcion)
values ('Gerencia', 1800, 'Gestión Estratégica');
insert into departamento (id, sueldo, descripcion)
values ('Administrativo', 900, 'Aréa administrativa');
insert into departamento (id, sueldo, descripcion)
values ('Comercial', 800, 'Aréa comercial');
insert into departamento (id, sueldo, descripcion)
values ('Recursos humanos', 800, 'Departamento de Recursos humanos');
insert into departamento (id, sueldo, descripcion)
values ('Operativo', 500, 'Parte operativa');

insert into cliente (ruc, nombre_empresa, direccion, telefono, actividad_principal, representante_legal)
values ('0944653782001', 'Ingenio San Carlos', 'Av. Principal o Av. San Carlos, Cnel. Marcelino Maridueña', '04-2321280', 'Azucarera', 'Mariano Gonzalez');
insert into cliente (ruc, nombre_empresa, direccion, telefono, actividad_principal, representante_legal)
values ('0945121437001', 'Papelera Nacional S.A.', 'Cnel. Marcelino Maridueña', '04-2729008', 'Distribuidor de papel', 'Pedro Gomez');


insert into proveedores (ruc, nombre, direccion)
values ('0912456378001', 'Repuestos Orozco', 'Sucre 1045 y 6 de marzo');
insert into proveedores (ruc, nombre, direccion)
values ('0978956128001', 'Textiles Ramirez', 'Alcedo y Lorenzo de Garaicoa');
insert into proveedores (ruc, nombre, direccion)
values ('0978465135001', 'TRZ Municiones', 'Tungurahua 1619 entre Ayacucho y Huancavilca');

/*insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)
values ('0910353639', 'Germán Antonio', 'Cruz Sanchéz', 'Casado', '1966-11-20', 'M', 'guardiagro.gcruz@gmail.com', '2002276363', 'activo', '2002-04-27', 'Gerencia', 'Gerente');
insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)
values ('0953548419', 'Lissete Lorena', 'Montalvo De La Torre', 'Casada', '1986-03-15', 'F', 'liss_montalvo@hotmail.com', '2010587963', 'activo', '2010-06-19', 'Administrativo', 'Asistente de Gerencia');
insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)
values ('0978228948', 'Stefany Gabriela', 'Quintero Franco', 'Casada', '1990-04-23', 'F', 'quintero_stefy@gmail.com', '2012894651', 'activo', '2012-05-22', 'Comercial', 'Contadora');
insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)
values ('0998561303', 'Pedro Guillermo', 'Ramirez Lopez', 'Casado', '1975-08-11', 'M', 'ramirez_pedro@hotmail.com', '2011456963', 'activo', '2011-02-12', 'Operativo', 'Guardia');
insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)
values ('0958412315', 'Alberto Rodrigo', 'Coello Calle', 'Soltero', '1990-06-30', 'M', 'alberto_coello@hotmail.com', '2016485367', 'activo', '2016-07-22', 'Operativo', 'Guardia');
insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)
values ('0967831950', 'Hector Edwin', 'Santacruz Arce', 'Casado', '1980-07-29', 'M', 'hecto_santacruz@gmail.com', '2011456963', 'activo', '2011-02-12', 'Operativo', 'Guardia');*/

insert into direcciones (cedula, provincia, canton, parroquia, descripcion)
values ('0910353639', 'Guayas', 'Daule', 'Las Lojas', 'Ubr Milann');
insert into direcciones (cedula, provincia, canton, parroquia, descripcion)
values ('0953548419', 'Guayas', 'Colimes', 'San Jacinto', 'Calle Simón Bolivar');
insert into direcciones (cedula, provincia, canton, parroquia, descripcion)
values ('0978228948', 'Guayas', 'El Empalme', 'El Rosario', 'Call Sucre y Tungurahua');
insert into direcciones (cedula, provincia, canton, parroquia, descripcion)
values ('0998561303', 'Guayas', 'Salitre','Salitre', 'Av 27 de noviembre y Damaso Contreras');
insert into direcciones (cedula, provincia, canton, parroquia, descripcion)
values ('0958412315', 'Guayas', 'Milagro','Chobo', 'Tungurahua y El Oro');
insert into direcciones (cedula, provincia, canton, parroquia, descripcion)
values ('0967831950', 'Guayas', 'Yaguachi','Virgen de Fátima', '6 marzo y Sucre');

insert into vacaciones (fecha_inicio, fecha_fin, cedula)
values ('2018-11-21', '2018-12-10', '0978228948');
insert into vacaciones (fecha_inicio, fecha_fin, cedula)
values ('2018-12-01', '2018-12-15', '0967831950');

insert into telefonos (numero, cedula)
values ('04-6023978', '0910353639');
insert into telefonos (numero, cedula)
values ('0939170937', '0953548419');
insert into telefonos (numero, cedula)
values ('04-6378852', '0953548419');
insert into telefonos (numero, cedula)
values ('04-2397573', '0978228948');
insert into telefonos (numero, cedula)
values ('04-2786453', '0998561303');
insert into telefonos (numero, cedula)
values ('04-2189241', '0958412315');
insert into telefonos (numero, cedula)
values ('04-2189241', '0967831950');

insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('1312623174', 'Anael', 'Reyes Rodriguez', 'anael-19@hotmail.com', 'P', '0953548419','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0922262282', 'Jose Kleber', 'Franco Ascencio', 'kleberfrancojr90@hotmail.com', 'E', '0953548419','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0910363837', 'Elizabeth', 'Terranova Sanchez', 'eterranova@hotmail.com', 'P', '0910353639','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0948745126', 'Juan Carlos', 'Osorio Freire', 'jcosorio@hotmail.com', 'E', '0910353639','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0912589634', 'Carlos Santiago', 'Perez', 'santiagoperez@hotmail.com', 'P', '0978228948','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('1355666324', 'Keila', 'Martinez Soza', 'kmartinez@hotmail.com', 'E', '0978228948','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('1896553986', 'Sonia', 'Freire', 'freire-sonia@hotmail.com', 'P', '0998561303','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0964631568', 'Maria Raquel', 'Soledispa Castro', 'marasole@hotmail.com', 'E', '0998561303','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0998523215', 'Nicole Angie', 'Guzman Portilla', 'nianguzm@hotmail.com', 'P', '0958412315','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('0974535896', 'Jesus Roxana', 'Navarro Valera', 'roxjenav@hotmail.com', 'E', '0958412315','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('1896439467', 'Omar Edwin', 'Rosales', 'omar-rosales@hotmail.com', 'P', '0967831950','0908153695');
insert into referencia (cedula, nombres, apellidos, email, tipo, cedula_empleado,telefono)
values ('1865189632', 'Juan Jose', 'Crow', 'jjcrow@hotmail.com', 'E', '0967831950','0908153695');


insert into pruebas_fisicas (id, nombre, resultado, cod_guardia)
values (1, 'Fueza', 'Requiere mayor entrenamiento', 1234);
insert into pruebas_fisicas (id, nombre, resultado, cod_guardia)
values (2, 'Fueza', 'Pefecto estado',1534 );
insert into pruebas_fisicas (id, nombre, resultado, cod_guardia)
values (3, 'Fueza', 'Pefecto estado', 1634);


insert into permisos (tipo_de_permiso, motivo, fecha_inicial, fecha_final, cedula)
values ('Descanso', 'Problema médico', '2018-11-24', '2018-12-02', '0998561303');
insert into permisos (tipo_de_permiso, motivo, fecha_inicial, fecha_final, cedula)
values ('Calamidad', 'Calamidad doméstica', '2018-10-22', '2018-10-24', '0978228948');

insert into cursos (id_curso, tema, lugar, duracion, cedula)
values (456, 'Primeros auxilios', 'Centro Médico XYZ', '2 días', '0958412315');
insert into cursos (id_curso, tema, lugar, duracion, cedula)
values (123, 'Primeros auxilios', 'Centro Médico XYZ', '2 días', '0998561303');

insert into productos (nombre, descripcion, ruc)
values ('Bujías', 'Repuesto de carro', '0912456378001');
insert into productos (nombre, descripcion, ruc)
values ('Discos', 'Repuesto de carro', '0912456378001');
insert into productos (nombre, descripcion, ruc)
values ('Camisetas', 'Camiseta de guardia', '0978956128001');
insert into productos (nombre, descripcion, ruc)
values ('Pantalones', 'Pantalón de guardia', '0978956128001');
insert into productos (nombre, descripcion, ruc)
values ('Municiones', 'Balas calibre 33', '0978465135001');
insert into productos (nombre, descripcion, ruc)
values ('Municiones xyz', 'Balas calibre 50', '0978465135001');


insert into telefono_proveedor (telefono, ruc)
values ('04-7892345', '0912456378001');
insert into telefono_proveedor (telefono, ruc)
values ('04-4898513', '0912456378001');
insert into telefono_proveedor (telefono, ruc)
values ('04-4894651', '0978956128001');
insert into telefono_proveedor (telefono, ruc)
values ('04-2589746', '0978465135001');

insert into insumos (cod_guardia, id_producto)
values (1234,1);
insert into insumos (cod_guardia, id_producto)
values (1234,2);
insert into insumos (cod_guardia, id_producto)
values (1234,3);
insert into insumos (cod_guardia, id_producto)
values (1534,4);
insert into insumos (cod_guardia, id_producto)
values (1534,5);
insert into insumos (cod_guardia, id_producto)
values (1534,6);
insert into insumos (cod_guardia, id_producto)
values (1634,2);
insert into insumos (cod_guardia, id_producto)
values (1634,1);
insert into insumos (cod_guardia, id_producto)
values (1634,5);
