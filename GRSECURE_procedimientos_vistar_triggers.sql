-- procedimiento de inicio de sesion
use grsecure;
delimiter //
create procedure inicioSesion (in ced varchar(10))
begin
select * from usuarios where cedula=ced;
end//
delimiter ;

create view vista_referencia_laboral as
select * from referencia natural join laboral
;

create view vista_referencia_familiares as 
select * from referencia natural join cargas_familiares;

-- vistas
create view vista_empleados as
select e.cedula, e.nombres, e.apellidos, d.descripcion from empleado e
join direcciones d on d.cedula=e.cedula;


create view vista_cliente as
select ruc as cedula, nombre_empresa as nombres, direccion as descripcion from cliente; 

create view vista_proveedores as
select ruc as cedula, nombre as nombres, direccion as descripcion from proveedores;

create view vista_perfil_guardia as
select * from empleado natural join guardia;

create view vista_GuardiasNoAsignados as
select e.cedula,e.nombres,e.apellidos,e.estado_civil
	from empleado e
	where e.cedula not in (select g.cedula
							from guardia g
                            join Cliente c on c.ruc = g.ruc)
                            and e.cargo = 'guardia';

delimiter //
create procedure datosResumenGuardia(in id varchar(13))
begin
select g.horario, e.cedula, e.nombres, e.apellidos from empleado e
join guardia g on e.cedula=g.cedula
where ruc=id
;
end //
delimiter ;


delimiter $$
create procedure ObtenerGuardiasxCliente(in rucCliente varchar(13))
begin
	select e.nombres,e.apellidos,e.cedula,g.horario
	from cliente c
	join guardia g on c.ruc = g.RUC
    join empleado e on g.cedula = e.cedula
    where c.ruc = rucCliente;
end$$

delimiter ;

delimiter //
create trigger trigger_referencias_eliminadas before delete on referencia
for each row 
begin 
delete from cargas_familiares where cedula=old.cedula;
delete from laboral where cedula=old.cedula;
end //
delimiter ;


create view vista_insumos_guardia as
select i.id, nombre, descripcion, cod_guardia from insumos i join productos p on p.id=i.id_producto;



delimiter $
create trigger trigger_elimar_referenciaGuardia before delete on Cliente
	for each row
		begin 
        update guardia set ruc = null where ruc = old.Ruc;
        end$
delimiter ;

delimiter //
create trigger trigger_eliminar_producto before delete on productos
for each row
begin
delete from insumos where id_producto=old.id;
end //
delimiter ;
