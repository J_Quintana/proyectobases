/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectobdlocal;

import controller.Login;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Diego Rojas
 */
public class ProyectoBDLocal extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FXMLLogin.fxml"));
        Parent root = loader.load();
        Login login = loader.getController();
        login.setPrincipal(stage);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("Sistema de gestión de recursos GUARDIAGRO");
        stage.setResizable(false);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
