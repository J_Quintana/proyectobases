/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Empleado;
import model.Permisos;

/**
 * FXML Controller class
 *
 * @author CEEMP02
 */
public class NuevoPermiso implements Initializable {

    @FXML
    private DatePicker dpInicial;
    @FXML
    private DatePicker dpFinal;
    @FXML
    private TextField txtTipo;
    @FXML
    private TextField txtMotivo;
    private String ci;
    private Stage actual, padre;
    private Empleado ep;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarDatos(Empleado ep,Stage actual, Stage padre) {
        this.padre=padre;
        this.actual = actual;
        this.ep = ep;
    }

    @FXML
    private void guardarPermiso() {
        if (verificarDatos()) {
            try {
                Permisos p = new Permisos(this.txtTipo.getText(), this.txtMotivo.getText(), dpInicial.getValue(), dpFinal.getValue(), 0, null,null);
                if (p.insertarDatos(this.ep.getCI())) {
                    util.Util.mostrarDialog("Se ingresó el dato con éxito");
                    this.padre.close();
                    this.ep.fabricarPerfilEmpleado();
                    this.actual.close();
                    
                }
            } catch (Exception e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            }
        }
    }

    private boolean verificarDatos() {
        if (txtMotivo.getText().equals("") || (txtMotivo.getText().length() > 50)) {
            util.Util.mostrarDialog("El motivo no es correcto solo puede tener 10 caracteres máximo");
            return false;
        } else if (txtTipo.getText().equals("") || (txtTipo.getText().length() > 10)) {
            util.Util.mostrarDialog("El tipo no puede tener más de 10 caracteres");
            return false;
        } else if (dpFinal.getValue() == null || dpInicial.getValue() == null) {
            util.Util.mostrarDialog("Llene todos los campos");
            return false;
        }
        else if(dpInicial.getValue().compareTo(dpFinal.getValue()) > 0){
            util.Util.mostrarDialog("La fecha de fin del permiso no puede ser inferior a la inicial");
            return false;
        }
        return true;
    }

}
