/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import model.Cliente;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class VerPerfilCliente implements Initializable {
    
    private Conexion conex;
    private PreparedStatement ps;
    private ResultSet res;
    private Connection con = null;
    ObservableList list = FXCollections.observableArrayList();
    private int acceso;

    @FXML
    private Label lbRUC;
    @FXML
    private Label lbActividad;
    @FXML
    private Label lbTelefono;
    @FXML
    private Label lbDireccion;
    @FXML
    private Label lbEmpresa;
    
    private Cliente cliente;
    @FXML
    private Label lblRepresentante;
    @FXML
    private ListView<String> tblvGuardias;
    @FXML
    private Button btnAgregarGuardiaAlCliente;
    @FXML
    private Button btnEliminarCliente;
    
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conex = new Conexion();
    }
    
    public void cargarDatosPerfil(Cliente cl,int accesibilidad){
        this.cliente = cl;
        this.acceso = accesibilidad;
        if(accesibilidad == 3){
            btnAgregarGuardiaAlCliente.setDisable(true);
        }
        
        
        lbRUC.setText(cliente.getRUC());
        lblRepresentante.setText(cliente.getRepresentante());
        lbActividad.setText(cliente.getActPrincipal());
        lbTelefono.setText(cliente.getTelefono());
        lbDireccion.setText(cliente.getDireccion());
        lbEmpresa.setText(cliente.getNombre());
        
        lbDireccion.setWrapText(true);
        //lbDireccion.setTextAlignment(TextAlignment.JUSTIFY);
        lbRUC.setWrapText(true);
        lblRepresentante.setWrapText(true);
        lbTelefono.setWrapText(true);
        lbEmpresa.setWrapText(true);
    }
    
    /**
    metodo que llena el list View con los empleados
    */
    public void llenarGuardias(Cliente cl){
        tblvGuardias.getItems().removeAll(list);
        list.removeAll(list);
        String datos;
        try {
            conex = new Conexion();
            con = conex.conectarMySQL();
            ps = con.prepareStatement("call ObtenerGuardiasxCliente(?)");
            ps.setString(1, cl.getRUC());
            res = ps.executeQuery();
            while (res.next()) {
                datos = res.getString("nombres")+"  "+res.getString("apellidos")+"  "+res.getString("cedula")+"  "+res.getString("horario");
                list.add(datos);
            }
            tblvGuardias.getItems().addAll(list);
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(VerPerfilCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        }
        
    }
    
    public void accionAbrirVentanaAgregarG(){
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLAgregarGuardia.fxml"));
            Parent root = fxmlLoader.load();
            AgregarGuardia controlador = fxmlLoader.getController();
            controlador.llenarTablaGuardiasDisponibles(this.cliente);
            Scene scene = new Scene(root);
            stage.setTitle("Agregar guardias al cliente");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
    
    public void accionEliminarCliente(){
         if(util.Util.confirmationDialog("¿Desea eliminar este cliente?")){
            try {
                conex = new Conexion();
                con = conex.conectarMySQL();
                ps = con.prepareStatement("delete from Cliente where ruc = "+this.cliente.getRUC());
                int n = ps.executeUpdate();
                if(n>0)
                    util.Util.mostrarDialog("Se elimino al cliente con éxito");
                else
                    util.Util.mostrarDialog("error al eliminar el cliente");
                con.close();
            } catch (SQLException e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            }finally {
                    if (con != null) {
                        try {
                            con.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(VerPerfilCliente.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
            }
         }
    
    }
    
    /*
    metodo que elimina al guardia que esta asginado a ese cliente
    */
    public void accionQuitarGuardia(MouseEvent evento){
        if(acceso == 3){
            util.Util.mostrarDialogAlert("no Tiene acceso");
        }
        if (evento.getClickCount() == 2) {
            String info = tblvGuardias.getSelectionModel().getSelectedItem();
            System.out.println(info);
            String [] datos = info.split(" ");

            String cedulaG = datos[6];
           if(util.Util.confirmationDialog("¿Desea quitar la asignación del guardia?")){

                try {
                    conex = new Conexion();
                    con = conex.conectarMySQL();
                    ps = con.prepareStatement("update guardia set ruc = null where cedula = "+cedulaG);
                    int n = ps.executeUpdate();
                    if(n>0)
                        util.Util.mostrarDialog("Se quito al guardia con éxito");
                    else
                        util.Util.mostrarDialog("error al quitar el guardia");
                    con.close();
                } catch (SQLException e) {
                    util.Util.mostrarDialogAlert(e.getMessage());
                }finally {
                        if (con != null) {
                            try {
                                con.close();
                            } catch (SQLException ex) {
                                Logger.getLogger(VerPerfilCliente.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        this.llenarGuardias(cliente);
                }
           }
        }
        
    
    }
    
    
}
