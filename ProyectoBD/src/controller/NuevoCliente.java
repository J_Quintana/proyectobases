/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Cliente;
import util.Util;

/**
 * FXML Controller class
 * Clase que maneja la ventana para Agregar un nuevo cliente a la base de datos
 * @author JONATHAN
 */
public class NuevoCliente implements Initializable {

    @FXML
    private TextField txtRUC;
    @FXML
    private TextField txtEmpresa;
    @FXML
    private TextField txtDireccion;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtActividad;
    @FXML
    private TextField txtRepresentante;
    @FXML
    private Button btnSave;
    private Cliente cliente;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    /**
     * metodo que valida que los datos ingresados sean correctos, crea una instancia de cliente y lo almacena en 
     * la base de datos
     */
    public void guardarCliente() {
        if(txtRUC.getText().equals("") || txtEmpresa.getText().equals("") || txtDireccion.getText().equals("") || txtTelefono.getText().equals("")
                || txtActividad.getText().equals("") || txtRepresentante.getText().equals("")){
            Util.mostrarDialog("Llene todos los campos para poder continuar");
        }
        else {
            if(!Util.verificarPatron("^[0-9]{10}001$", txtRUC.getText())){
                Util.mostrarDialog("El ruc no tiene el formato adecuado");
                txtRUC.requestFocus();
            }
            else if(!Util.verificarPatron("^[0-9]{9,10}$", txtTelefono.getText())){
                Util.mostrarDialog("El teléfono no tiene el formato adecuado");
                txtTelefono.requestFocus();
            }
            else if (!Util.verificarPatron(".{1,150}", txtEmpresa.getText())){
                Util.mostrarDialog("El nombre de la empresa no puede tener más de 150 caracteres");
                txtEmpresa.requestFocus();
            }
            else if (!Util.verificarPatron(".{1,200}", txtDireccion.getText())){
                Util.mostrarDialog("La dirección de la empresa no puede tener más de 200 caracteres");
                txtDireccion.requestFocus();
            }
            else if (!Util.verificarPatron(".{1,50}", txtActividad.getText())){
                Util.mostrarDialog("La actividad de la empresa no puede tener más de 50 caracteres");
                txtActividad.requestFocus();
            }
            else if (!Util.verificarPatron(".{1,100}", txtRepresentante.getText())){
                Util.mostrarDialog("El representante de la empresa no puede tener más de 100 caracteres");
                txtRepresentante.requestFocus();
            }
            else {
                cliente = new Cliente(txtRUC.getText(),txtEmpresa.getText(),txtDireccion.getText(),txtTelefono.getText(),
                txtActividad.getText(),txtRepresentante.getText());
                try {
                    if(cliente.insertDatos()){
                        util.Util.mostrarDialog("Se ingresó el cliente con éxito!!");
                        borrarCajas();
                    }
                    else {
                        util.Util.mostrarDialog("No se pudo ingresar el cliente");
                    }
                }
                catch(Exception e){
                    util.Util.mostrarDialogAlert(e.getMessage());
                }
            }
        }
    }
    
    private void borrarCajas(){
        txtRUC.clear();
        txtEmpresa.clear();
        txtDireccion.clear();
        txtTelefono.clear();
        txtActividad.clear();
        txtRepresentante.clear();
        txtRUC.requestFocus();
    }
    
}
