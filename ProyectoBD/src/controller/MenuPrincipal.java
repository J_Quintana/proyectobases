/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Cliente;
import model.Empleado;
import model.FactoryTableContent;
import model.Guardia;
import model.Proveedores;
import model.ResumeGuardia;
import model.Usuario;

/**
 * FXML Controller class
 * Clase que controla la ventana del menu principal
 * @author Lenovo comp
 */
public class MenuPrincipal implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ImageView imgUser;
    private Usuario user;
    @FXML
    private Label lbNombre;
    @FXML
    private Label lbCargo;
    private Stage principal, actual;
    @FXML
    private TableView<FactoryTableContent> tbDatos;
    @FXML
    private TextField txtBusqueda;
    @FXML
    private TableColumn<FactoryTableContent, String> colID;
    @FXML
    private TableColumn<FactoryTableContent, String> colNombre;
    @FXML
    private TableColumn<FactoryTableContent, String> colDireccion;
    private Conexion conex;
    private Connection con = null;
    private PreparedStatement ps;
    private ResultSet res;
    @FXML
    private Button searchEmpleado;
    @FXML
    private Button searchCliente;
    @FXML
    private Button searchProveedor;
    private int tCasting;
    @FXML
    private MenuItem btnNuevoEmpleado;
    @FXML
    private MenuItem btnNuevoCliente;
    @FXML
    private MenuItem btnBuscarCliente;
    @FXML
    private MenuItem btnNuevoProveedor;
    @FXML
    private MenuItem btnBuscarProveedor;
    @FXML
    private Label lbTitulo;
    @FXML
    private Menu menuEmpleado;
    @FXML
    private Menu menuCliente;
    @FXML
    private Menu menuProveedor;

    private ObservableList<FactoryTableContent> tablaCliente;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO 
        conex = new Conexion();
    }
    //

    public void setUser(Usuario user) {
        this.user = user;
    }

    public void setPrincipal(Stage principal) {
        this.principal = principal;
    }

    public void setActual(Stage actual) {
        this.actual = actual;
    }
    
    /**
     * metodo que carga los datos del usuario que ingreso en la parte izquierda de la ventana
     * y controla sus actividades segun el nivel de acceso que tenga el usuario
     */

    public void cargarDatos() {
        this.lbNombre.setText(this.user.getNombre());
        String rol;
        switch (this.user.getRol()) {
            case 1:
                rol = "Gerente";
                break;
            case 2:
                rol = "Asistente";
                btnNuevoEmpleado.setVisible(false);
                btnNuevoCliente.setVisible(false);
                btnBuscarCliente.setVisible(false);
                btnNuevoProveedor.setVisible(false);
                btnBuscarProveedor.setVisible(false);
                menuCliente.setVisible(false);
                menuProveedor.setVisible(false);
                break;
            default:
                rol = "Contadora";
                btnNuevoProveedor.setVisible(false);
                btnNuevoEmpleado.setVisible(false);
                btnBuscarProveedor.setVisible(false);
                menuProveedor.setVisible(false);
                break;
        }
        this.lbCargo.setText(rol);
        try {
            if (util.Util.Base64ToFile(this.user.getFoto(), "fotoperfil.jpg")) {
                File path = new File(".\\src\\resources\\tmp\\fotoperfil.jpg");
                Image imagen = new Image(path.toURI().toString());
                this.imgUser.setImage(imagen);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    //
    private void nuevaVentana(String nombre, String titulo) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/" + nombre + ".fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.setTitle(titulo);
            stage.setScene(scene);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

    @FXML
    private void cerrarSesion() {
        this.principal.show();
        this.actual.close();
    }

    @FXML
    private void buscarEmpleado() {
        try {
            String query = "select * from vista_empleados where cedula like '%" + txtBusqueda.getText() + "%'"
                    + " or nombres like '%" + txtBusqueda.getText() + "%' or apellidos like '%" + txtBusqueda.getText()
                    + "%'";
            cargarDatosTablaClientes(query, true);
            tCasting = 1;
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    @FXML
    private void buscarCliente() {
        try {
            String query = "select * from vista_cliente where cedula like '%" + txtBusqueda.getText() + "%'"
                    + " or nombres like '%" + txtBusqueda.getText() + "%'";
            cargarDatosTablaClientes(query, false);
            tCasting = 2;
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    @FXML
    private void buscarProveedor() {
        try {
            String query = "select * from vista_proveedores where cedula like '%" + txtBusqueda.getText() + "%'"
                    + " or nombres like '%" + txtBusqueda.getText() + "%'";
            cargarDatosTablaClientes(query, false);
            tCasting = 3;
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }
    
    /*
    metodo que realiza los querys en el menu principal, res es quien tiene 
    */

    private void cargarDatosTablaClientes(String query, boolean bandera) throws SQLException {
        if (txtBusqueda.getText().equals("")) {
            util.Util.mostrarDialog("Debe indicar el valor de la búsqueda");
            txtBusqueda.requestFocus();
        } else {
            try {
                con = conex.conectarMySQL();
                ps = con.prepareStatement(query);
                res = ps.executeQuery();
                tablaCliente = FXCollections.observableArrayList();
                
                if (bandera) {
                    while (res.next()) {
                        tablaCliente.add(new FactoryTableContent(res.getString("nombres"), res.getString("apellidos"), res.getString("cedula"), res.getString("descripcion")));
                    }
                } else {
                    while (res.next()) {
                        tablaCliente.add(new FactoryTableContent(res.getString("nombres"), "", res.getString("cedula"), res.getString("descripcion")));
                    }
                }
                colID.setCellValueFactory(cellData -> cellData.getValue().getCi());
                colNombre.setCellValueFactory(cellData -> cellData.getValue().getName());
                colDireccion.setCellValueFactory(cellData -> cellData.getValue().getDireccion());
                tbDatos.setItems(tablaCliente);
                con.close();
                if (tbDatos.getItems().isEmpty()) {
                    util.Util.mostrarDialog("No hay datos a mostrar");
                }
            } catch (Exception e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            } finally {
                if (con != null) {
                    con.close();
                }
            }
        }
    }

    @FXML
    private void nuevoUsuario() {
        this.nuevaVentana("FXMLNuevoUsuario", "Registro de administradores de base de datos");

    }

    private boolean tieneGuardia(String ruc) throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select cedula from guardia where RUC=?");
            ps.setString(1, ruc);
            res = ps.executeQuery();
            return res.next();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private Cliente fabricarCliente(String ruc) {
        Cliente c = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(Cliente.querySimple(ruc));
            res = ps.executeQuery();
            if (res.next()) {
                c = new Cliente(res.getString("ruc"), res.getString("nombre_empresa"), res.getString("direccion"),
                        res.getString("telefono"), res.getString("actividad_principal"), res.getString("representante_legal"));
                con.close();
                if (tieneGuardia(ruc)) {
                    con = conex.conectarMySQL();
                    ps = con.prepareStatement(Cliente.queryResumeGuardia(ruc));
                    res = ps.executeQuery();
                    while (res.next()) {
                        c.addResumeGuardia(new ResumeGuardia(res.getString("horario"), res.getString("nombres"), res.getString("apellidos"), res.getString("cedula")));
                    }
                    con.close();
                }
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        return c;
    }
    /*
    metodo que carga la informacion al controller cliente y de ahi llama a los metodos dentro de controller
    si user get rol == 1 entonces el usuario es gerente
    si user get rol == 2 entonces el usuario es asistente
    si user get rol == 1 entonces el usuario es contadora
    */
    private void fabricarPerfilCliente(Cliente cl) {
        if(user.getRol() == 2){
            util.Util.mostrarDialogAlert("no tiene acceso");
        }
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilCliente.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilCliente pcl = fxmlLoader.getController();
            pcl.cargarDatosPerfil(cl,user.getRol()); 
            pcl.llenarGuardias(cl);
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Datos de la empresa: " + cl.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            System.out.println(i.getMessage());
        }
    }

    @FXML
    private void menuEBuscar() {
        searchEmpleado.setVisible(true);
        searchCliente.setVisible(false);
        searchProveedor.setVisible(false);
        txtBusqueda.setVisible(true);
        tbDatos.setVisible(true);
        lbTitulo.setText("Búsqueda de Empleado.");
        lbTitulo.setVisible(true);
        txtBusqueda.clear();
        if (tablaCliente != null) {
            tablaCliente.clear();
        }

    }

    @FXML
    private void menuENuevo() {
        this.nuevaVentana("FXMLNuevoEmpleado", "Registro de empleado");
    }

    @FXML
    private void menuCBuscar() {
        searchEmpleado.setVisible(false);
        searchCliente.setVisible(true);
        searchProveedor.setVisible(false);
        txtBusqueda.setVisible(true);
        tbDatos.setVisible(true);
        lbTitulo.setText("Búsqueda de Cliente.");
        lbTitulo.setVisible(true);
        txtBusqueda.clear();
        if (tablaCliente != null) {
            tablaCliente.clear();
        }

    }

    @FXML
    private void menuCNuevo() {
        this.nuevaVentana("FXMLNuevoCliente", "Registro de cliente");
    }

    @FXML
    private void menuPBuscar() {
        searchEmpleado.setVisible(false);
        searchCliente.setVisible(false);
        searchProveedor.setVisible(true);
        txtBusqueda.setVisible(true);
        tbDatos.setVisible(true);
        lbTitulo.setText("Búsqueda de Proveedor.");
        lbTitulo.setVisible(true);
        txtBusqueda.clear();
        if (tablaCliente != null) {
            tablaCliente.clear();
        }

    }

    @FXML
    private void menuPNuevo() {
        this.nuevaVentana("FXMLNuevoProveedor", "Registro de proveedor");
    }

    /*
    metodo que con el evento de dar 2 clic en un empleado etc. se abra la nueva ventana de 
    ver el perfil de empleado,cliente etc
    */
    @FXML
    private void crearUsuario(MouseEvent event) {
        if (event.getClickCount() == 2) {
            FactoryTableContent usuarioSelec = tbDatos.getSelectionModel().getSelectedItem();
            String codigo = usuarioSelec.getCi().getValue();
            try {
                if (util.Util.confirmationDialog("¿Desea ver el perfil del usuario?")) {
                    switch (tCasting) {
                        case 1:
                            fabricarPerfilEmpleado(codigo);
                            break;
                        case 2:
                            Cliente cliente = fabricarCliente(codigo);
                            this.fabricarPerfilCliente(cliente);
                            break;
                        default:
                            Proveedores p = this.fabricarProveedor(codigo);
                            this.fabricarPerfilProveedor(p);
                            break;
                    }
                }
            } catch (Exception e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            }
        }
    }

    private Proveedores fabricarProveedor(String ruc) {
        Proveedores p = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(Proveedores.querySimple(ruc));
            res = ps.executeQuery();
            if (res.next()) {
                p = new Proveedores(res.getString("ruc"), res.getString("nombre"), res.getString("direccion"));
            }
            res.close();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        return p;
    }

    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            pcl.setPadre(stage);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

    private void fabricarPerfilEmpleado(String ci) {
        Empleado emp;
        boolean bandera = true;
        try {
            if (esGuardia(ci)) {
                emp = crearGuardia(ci);
            } else {
                emp = crearEmpleado(ci);
                bandera = false;
            }
            if (emp != null) {
                Stage stage = new Stage();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilEmpleado.fxml"));
                Parent root = fxmlLoader.load();
                VerPerfilEmpleado pe = fxmlLoader.getController();
                pe.cargarDatos(emp, bandera, stage);
                emp.setPadre(stage);
                Scene scene = new Scene(root);
                stage.setTitle("Datos del Empleado: " + emp.getNombres());
                stage.setScene(scene);
                stage.setResizable(false);
                stage.show();
            } else {
                System.out.println("Empleado null");
            }

        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private Boolean esGuardia(String ci) {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            return res.next();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    private LinkedList<String> cargarTelefonos(String ci) {
        LinkedList<String> telefonos = new LinkedList<>();
        Connection con2 = null;
        PreparedStatement ps2;
        ResultSet res2;
        try {
            con2 = conex.conectarMySQL();
            ps2 = con2.prepareStatement("select numero from telefonos where cedula=?");
            ps2.setString(1, ci);
            res2 = ps2.executeQuery();
            while (res2.next()) {
                telefonos.add(res2.getString("numero"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (con2 != null) {
                    con2.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return telefonos;
    }

    private Guardia crearGuardia(String ci) throws SQLException {
        Guardia g = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_perfil_guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                g = new Guardia(res.getInt("cod_guardia"), res.getString("credencial"), res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
                g.setHorario(res.getString("horario"));
                g.setRuc(res.getString("RUC"));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return g;
    }

    private Empleado crearEmpleado(String ci) {
        Empleado emp = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from empleado where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                emp = new Empleado(res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        return emp;
    }

}
