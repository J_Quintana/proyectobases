/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Guardia;
import model.PruebasFisicas;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevaPruebaFisica implements Initializable {

    @FXML
    private TextField txtName;
    private Guardia g;
    @FXML
    private TextArea taResultado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void cargarDatos(Guardia g){
        this.g=g;
    }
    

    @FXML
    private void GuardarFoto() {
        if(txtName.getText().equals("") || taResultado.getText().equals("")){
            util.Util.mostrarDialog("Llene todos los campos antes de continuar");
        }
        else if(!util.Util.verificarPatron(".{1,100}", txtName.getText())){
            util.Util.mostrarDialog("El nombre debe tener mínimo 1 caracter y máximo 100");
            txtName.requestFocus();
        }
        else if(!util.Util.verificarPatron(".{1,300}", taResultado.getText())){
            util.Util.mostrarDialog("El resultado puede tener máximo 300 caracteres y mínimo 1");
            taResultado.requestFocus();
        }
        else {
            try {
                PruebasFisicas pf = new PruebasFisicas(0,txtName.getText(),taResultado.getText(),this.g);
                if(pf.insertar()){
                    util.Util.mostrarDialog("Se registró la prueba con éxito, para ver los cambios actualice la ventana");
                }
                else {
                    util.Util.mostrarDialog("No se pudo registrar la prueba");
                }
            }
            catch(Exception ex){
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        }
    }
    
}
