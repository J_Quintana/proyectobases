/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Cliente;
import model.FactoryTableContent;
import util.Util;

/**
 * FXML Controller class
 *controlador de la vista para agregar un guardia al cliente
 * @author Diego Rojas
 */
public class AgregarGuardia implements Initializable {
    
    private Conexion conex;
    private PreparedStatement ps;
    private ResultSet res;
    private Connection con = null;
    ObservableList list = FXCollections.observableArrayList();
    
    @FXML
    private TextField txtCedula;
    @FXML
    private TextField txtHorario;
    @FXML
    private Button btnAgregar;
    @FXML
    private ListView<String> lvGuardias;
    
    private Cliente c;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * metodo que llena la lista con los datos de los guardias que no han sido asignados a ningun cliente
     * @param Cliente
     */
    public void llenarTablaGuardiasDisponibles(Cliente c){
        this.c = c;
        list.removeAll(list);
        String query = "select * from vista_GuardiasNoAsignados";
        String s;
        try {
            conex = new Conexion();
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            res = ps.executeQuery();
           while (res.next()) {
                s = res.getString("nombres")+"  "+res.getString("apellidos")+"  "+res.getString("cedula");
                list.add(s);
           }
           lvGuardias.getItems().addAll(list);
            con.close();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(VerPerfilCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        }
        
        
    
    }
    /**
     * metodo que asigna un guardia al cliente
    */
    public void accionAgregarGuardia(){
        if(this.validarHorario() && Util.verificarPatron("^[0-9]{2,10}$", txtCedula.getText())){
            try {
            conex = new Conexion();
            con = conex.conectarMySQL();
            //    System.out.println("el ruc"+c.getRUC());
            //horario = "+this.txtHorario.getText()+",
            //ps = con.prepareStatement("update guardia set ruc = "+c.getRUC()+" , horario = "+this.txtHorario.getText()+" where cedula = "+this.txtCedula.getText());
            ps = con.prepareStatement("update guardia set ruc = '"+c.getRUC()+ "',horario = '"+this.txtHorario.getText()+"' where cedula = '"+ this.txtCedula.getText()+"'");
            int n =  ps.executeUpdate();
            if(n>0)
                util.Util.mostrarDialog("Se agrego al guardia con éxito");
            else
                util.Util.mostrarDialog("error al asignar el guardia");
                

            con.close();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(VerPerfilCliente.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        }
        }else{
            util.Util.mostrarDialog("Formato erroneo en el horario o en cedula");
        }
    
    }
    
    
    private boolean validarHorario(){
        if(this.txtHorario.getText().equals(""))
            return false;
        String[] horario = this.txtHorario.getText().split("-");
        if(horario == null || horario.length !=2)
            return false;
        int hi = Integer.parseInt(horario[0].substring(0, 2));
        int h2 = Integer.parseInt(horario[1].substring(0, 2));
        if(h2<hi)
            return false;
        return true;
        
    }

 //lave de clase   
}
