/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Factura;
import model.Productos;
import model.Proveedores;
import model.Telefono_Proveedores;

/**
 * FXML Controller class
 *
 * @author Comp
 */
public class VerPerfilProveedor implements Initializable {

    @FXML
    private Label lbRUC;
    @FXML
    private Label lbNombre;
    @FXML
    private Label lbDireccion;
    @FXML
    private VBox vbTelefonos;
    private Proveedores proveedor;
    @FXML
    private Label lbTitulo;
    private Stage padre;
    @FXML
    private VBox vbProveedores;
    @FXML
    private HBox hbFacturas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

    }

    public void cargarDatosProveedor(Proveedores p) {
        this.proveedor = p;
        vbTelefonos.setSpacing(15);
        vbProveedores.setSpacing(15);
        hbFacturas.setSpacing(15);
        lbRUC.setText(this.proveedor.getRuc());
        lbTitulo.setText("Perfil del Proveedor: " + this.proveedor.getNombre());
        lbNombre.setText(proveedor.getNombre());
        StringBuilder direccion = new StringBuilder();
        for (int i = 0; i < proveedor.getDireccion().length(); i++) {
            if (i % 64 == 0) {
                direccion.append("\n");
            }
            direccion.append(proveedor.getDireccion().charAt(i));
        }
        lbDireccion.setText(direccion.toString());
        try {
            if (proveedor.tieneTelefonos() && proveedor.graficarTelefonos()) {
                LinkedList<Telefono_Proveedores> tele = proveedor.getTel();
                for (Telefono_Proveedores t : tele) {
                    vbTelefonos.getChildren().add(t.dibujarNumero());
                }
            } else {
                System.out.println("no entre");
            }
            if (proveedor.tieneProductos() && proveedor.graficarProductos()) {
                LinkedList<Productos> productos = proveedor.getProductos();
                for (Productos pro : productos) {
                    vbProveedores.getChildren().add(pro.dibujarProducto());
                }
            } else {
                System.out.println("No entre en productos");
            }
            if (proveedor.tieneFacturas() && proveedor.graficarFacturas()) {
                LinkedList<Factura> facturas = proveedor.getFacturas();
                for (Factura f : facturas) {
                   hbFacturas.getChildren().add(f.dibujarFactura());
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(VerPerfilProveedor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void setPadre(Stage padre) {
        this.padre = padre;
    }

    @FXML
    private void nuevoNumero() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoTelefono.fxml"));
            Parent root = fxmlLoader.load();
            NuevoTelefono controlador = fxmlLoader.getController();
            controlador.setQuery("insert into telefono_proveedor values(?,?)");
            controlador.setId(this.proveedor.getRuc());
            controlador.setStage(stage);
            controlador.setBandera(true);
            controlador.setObjeto(this.proveedor);
            controlador.setStagePadre(this.padre);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Número");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void NuevoProducto() {
        try {
            Stage stage = new Stage();
            proveedor.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoProducto.fxml"));
            Parent root = fxmlLoader.load();
            NuevoProducto pcl = fxmlLoader.getController();
            pcl.setPadre(padre);
            pcl.setHijo(stage);
            pcl.setBandera(true);
            pcl.setProveedor(proveedor);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + proveedor.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

    @FXML
    private void nuevaFactura() {
        try {
            Stage stage = new Stage();
            proveedor.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaFactura.fxml"));
            Parent root = fxmlLoader.load();
            NuevaFactura controlFactura = fxmlLoader.getController();
            controlFactura.setPadre(padre);
            controlFactura.setHijo(stage);
            controlFactura.setProveedor(proveedor);
            Scene scene = new Scene(root);
            stage.setTitle("Registro de factura del Proveedor: " + proveedor.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        }
        catch(IOException e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    @FXML
    private void eliminarProveedor() {
        if(util.Util.confirmationDialog("Seguro que desea eliminar el proveedor")){
            try {
                if(this.proveedor.eliminar()){
                    util.Util.mostrarDialog("Se elimino el proveedor con exito, los cambios se veran reflejados cuando actualice el sistema");
                }
                else{
                    util.Util.mostrarDialog("No se pudo eliminar el proveedor");
                }
            }
            catch(Exception ex){
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        }
    }

}
