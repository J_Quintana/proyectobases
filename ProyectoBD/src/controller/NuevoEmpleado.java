/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import model.Empleado;
import model.Guardia;
import util.Util;

/**
 * FXML Controller class
 *Clase que contrala la ventana para agregar un nuevo empleado
 * @author JONATHAN
 */
public class NuevoEmpleado implements Initializable {

    @FXML
    private TextField txtCI;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtApellido;
    @FXML
    private ComboBox cmbEstado;
    @FXML
    private DatePicker dpFechaNacimiento;
    @FXML
    private CheckBox checGenero;
    @FXML
    private CheckBox checEstado;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtSeguro;
    @FXML
    private DatePicker dpFechaIngreso;
    @FXML
    private ComboBox cmbDepartamento;
    @FXML
    private ComboBox cmbCargo;
    @FXML
    private TextField txtTelefono;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnFoto;
    @FXML
    private Button btnSave;
    private LinkedList<String> telefonos;
    private File imgFile;
    @FXML
    private Label lbContador;
    private List<String> departamentos;
    @FXML
    private CheckBox checGuardia;
    private Empleado empleado;
    @FXML
    private AnchorPane panelGuardia;
    @FXML
    private TextField txtCodigo;
    @FXML
    private Button btnCredencial;
    private File credencial;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        departamentos = util.Util.crearListaDepartamentos();
        telefonos = new LinkedList<>();
        cmbEstado.getItems().addAll( "Soltero","Casado","Divorciado","Unión libre");
        cmbDepartamento.getItems().addAll(departamentos);
        cmbCargo.getItems().addAll("Empleado","Gerente","Contador","RR HH");
    }    



    @FXML
    private void CargarNumero() {
        String tmp = "";
        tmp = txtTelefono.getText();
        if(Util.verificarPatron("^((0[1-9])[0-9]{7,8})$", tmp)){
            telefonos.add(tmp);
            txtTelefono.setText("");
            lbContador.setText(""+telefonos.size());
        }
        else {
            Util.mostrarDialog("El número ingresado es incorrecto");
            txtTelefono.setText("");
            txtTelefono.requestFocus();
        }
    }

    @FXML
    private void cargarFoto() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Buscar Imagen");
        // Agregar filtros para facilitar la busqueda
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.jpeg", "*.jpg", "*.png", "*.bmp", "*.gif")
        );
        // Obtener la imagen seleccionada
        imgFile = fileChooser.showOpenDialog(null);
    }

    @FXML
    private void GuardarDatos() {
        if(txtCI.getText().equals("") || txtNombre.getText().equals("") || txtApellido.getText().equals("") || txtEmail.getText().equals("")
                ||txtSeguro.getText().equals("") || telefonos.isEmpty() || cmbCargo==null || cmbDepartamento==null
                || cmbEstado== null || dpFechaIngreso.getValue() == null || dpFechaNacimiento.getValue()==null || imgFile==null){
            Util.mostrarDialog("LLene todos los campos para poder guardar la información");
        }
        else {
            if(dpFechaNacimiento.getValue().compareTo(LocalDate.now().minusDays(6570)) > 0){
                Util.mostrarDialog("Recuerde que debe ser mayor de edad para poder registrarlo como empleado");
                dpFechaNacimiento.requestFocus();
            }
            else if (dpFechaIngreso.getValue().compareTo(LocalDate.now()) > 0){
                Util.mostrarDialog("Recuerde que la fecha de ingreso a la empresa no puede ser mayor que el día actual");
                dpFechaIngreso.requestFocus();
            }
            else if(!Util.verificarPatron("^([a-z][a-z A-Z 0-9 \\_ \\. \\-]+@[a-z A-Z]+.(com)(\\.ec)?)", txtEmail.getText()) || !Util.verificarPatron(".{1,150}", txtEmail.getText())){
                Util.mostrarDialog("El correo tiene un formato incorrecto");
                txtEmail.requestFocus();
            }
            else if(!Util.verificarPatron("^[0-9]{2,10}$", txtCI.getText())){
                Util.mostrarDialog("El formato de la cédula no es el apropiado");
                txtCI.requestFocus();
            }
            else if(!Util.verificarPatron("^[0-9 a-z A-Z]{10}$", txtSeguro.getText())){
                Util.mostrarDialog("El formato del seguro no es adecuado");
                txtSeguro.requestFocus();
            }
            else if(!util.Util.verificarPatron(".{1,150}", txtNombre.getText())){
                Util.mostrarDialog("Recuerde que el nombre no puede tener más de 150 caracteres");
                txtNombre.requestFocus();
            }
            else if(!util.Util.verificarPatron(".{1,150}", txtApellido.getText())){
                Util.mostrarDialog("Recuerde que el apellido no puede tener más de 150 caracteres");
                txtApellido.requestFocus();
            }
            else {
                int index = 0;
                int i =0;
                for(String s : departamentos){
                    if(s.equals(cmbDepartamento.getValue()))
                        index = i;
                    i++;
                }
                if(checGuardia.isSelected()){
                    if(txtCodigo.getText().equals("") || credencial==null){
                        Util.mostrarDialog("LLene todos los campos para poder guardar la información");
                    }
                    else {
                        if(!Util.verificarPatron("[0-9]{4,10}", txtCodigo.getText())){
                        Util.mostrarDialog("El código del guardia solo puede ser numérico y tener minimo 4");
                        txtCodigo.requestFocus();
                        }
                        else {
                            empleado = new Guardia(Integer.parseInt(txtCodigo.getText()),Util.FileToBase64(credencial),txtNombre.getText(),txtApellido.getText(),txtCI.getText(),
                            cmbEstado.getValue().toString(),dpFechaNacimiento.getValue(),dpFechaIngreso.getValue(),checGenero.isSelected(),
                            checEstado.isSelected(),txtEmail.getText(),txtSeguro.getText(),cmbDepartamento.getValue().toString(),
                            cmbCargo.getValue().toString(),Util.FileToBase64(imgFile),telefonos);
                            try {
                                if(empleado.insertDatos() && empleado.inserTelefonos()){
                                    util.Util.mostrarDialog("Se ingresó el guardia con éxito");
                                    borrarDatos();
                                }
                                else
                                    util.Util.mostrarDialogAlert("No se ingresó el empleado");
                            }
                            catch(Exception e){
                                util.Util.mostrarDialogAlert(e.getMessage());
                            }
                        }
                    }
                    
                }
                else {
                    empleado = new Empleado(txtNombre.getText(),txtApellido.getText(),txtCI.getText(),
                    cmbEstado.getValue().toString(),dpFechaNacimiento.getValue(),dpFechaIngreso.getValue(),checGenero.isSelected(),
                    checEstado.isSelected(),txtEmail.getText(),txtSeguro.getText(),cmbDepartamento.getValue().toString(),
                    cmbCargo.getValue().toString(),Util.FileToBase64(imgFile),telefonos);
                    try {
                        if(empleado.insertDatos() && empleado.inserTelefonos()){
                            util.Util.mostrarDialog("Se ingresó el empleado con éxito");
                            borrarDatos();
                        }
                        else
                            util.Util.mostrarDialogAlert("No se ingresó el empleado");
                    }
                    catch (Exception e){
                        util.Util.mostrarDialogAlert(e.getMessage());
                    }
                }
            }
        } 
    } 
    
    @FXML
    private void activarGuardia() {
        panelGuardia.setVisible(checGuardia.isSelected());
    }

    @FXML
    private void addCredencial() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Buscar Imagen");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.jpeg", "*.jpg", "*.png", "*.bmp", "*.gif")
        );
        credencial = fileChooser.showOpenDialog(null);
    }
    
    private void borrarDatos(){
        txtCI.clear();
        txtApellido.clear();
        txtCodigo.clear();
        txtEmail.clear();
        txtNombre.clear();
        txtSeguro.clear();
        txtTelefono.clear();
        cmbCargo.setValue(null);
        cmbDepartamento.setValue(null);
        cmbEstado.setValue(null);
        dpFechaIngreso.setValue(null);
        dpFechaNacimiento.setValue(null);
        checEstado.setSelected(false);
        checGenero.setSelected(false);
        checGuardia.setSelected(false);
        telefonos.clear();
        imgFile = null;
        credencial = null;
        lbContador.setText("0");
        panelGuardia.setVisible(false);
        empleado = null;
        
    }
}
