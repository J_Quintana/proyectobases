/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Proveedores;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoProducto implements Initializable {

    @FXML
    private TextField txtNombre;
    @FXML
    private TextArea txtDescripcion;
    private String id;
    private boolean bandera;// true nuevo, false update
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private Stage padre, hijo;
    private Proveedores proveedor;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conex = new Conexion();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    public void setProveedor(Proveedores proveedor) {
        this.proveedor = proveedor;
    }

    public void setPadre(Stage padre) {
        this.padre = padre;
    }

    public void setHijo(Stage hijo) {
        this.hijo = hijo;
    }
    
    public void registrarTextBox(String nombre, String descripcion){
        this.txtDescripcion.setText(descripcion);
        txtNombre.setText(nombre);
    }

    @FXML
    private void guardarDatos() {
        if (txtNombre.getText().equals("") || txtDescripcion.getText().equals("")) {
            util.Util.mostrarDialog("Llene todos los campos..");
        } else if (!util.Util.verificarPatron("^.{1,100}", txtNombre.getText())) {
            util.Util.mostrarDialog("El nombre no tiene el formato adecuado, no puede tener más de 100 caracteres");
            txtNombre.requestFocus();
        } else if (!util.Util.verificarPatron("^.{1,200}", txtDescripcion.getText())) {
            util.Util.mostrarDialog("La descripción no tiene el formato adecuado, no puede tener más de 200 caracteres");
            txtDescripcion.requestFocus();
        } else {
            try {
                if (bandera) {
                    this.registrarProducto();
                }
                else {
                    this.updateProducto();
                }
            } catch (SQLException e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            }
        }
    }

    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            pcl.setPadre(stage);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

    private void registrarProducto() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into productos(nombre,descripcion,ruc) values(?,?,?)");
            ps.setString(1, txtNombre.getText());
            ps.setString(2, txtDescripcion.getText());
            ps.setString(3, this.proveedor.getRuc());
            if (ps.executeUpdate() > 0) {
                util.Util.mostrarDialog("Se registró el producto con éxito");
                this.padre.close();
                this.hijo.close();
                this.fabricarPerfilProveedor(proveedor);
            } else {
                util.Util.mostrarDialog("No se registró el producto");
            }
        }
        catch(SQLException e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
    }
    
    private void updateProducto() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("update productos set nombre=?, descripcion=? where id=?");
            ps.setString(1, txtNombre.getText());
            ps.setString(2, txtDescripcion.getText());
            ps.setString(3, id);
            if (ps.executeUpdate() > 0) {
                util.Util.mostrarDialog("Se actualizó el producto con éxito");
                this.padre.close();
                this.hijo.close();
                this.fabricarPerfilProveedor(proveedor);
            } else {
                util.Util.mostrarDialog("No se actualizó el producto");
            }
        }
        catch(SQLException e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
    }

}
