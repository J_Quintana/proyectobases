/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.CargaFamiliar;
import model.Empleado;
import model.Referencia;
import model.ReferenciaLaboral;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevaReferencia implements Initializable {

    @FXML
    private TextField txtCI;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtApellidos;
    @FXML
    private TextField txtMail;
    @FXML
    private ComboBox cmbCargar;
    @FXML
    private Button btnSave;
    @FXML
    private AnchorPane panel;
    @FXML
    private AnchorPane PanelLaboral;
    @FXML
    private TextField txtCarga;
    @FXML
    private TextField txtEmpresa;
    @FXML
    private AnchorPane PanelFamiliar;
    @FXML
    private DatePicker dpNacimiento;
    @FXML
    private TextField txtOcupacion;
    @FXML
    private ComboBox cmbtipoCarga;
    @FXML
    private TextField txtNumero;
    private String telefonos;
    private Referencia referencia;
    private Empleado emp;
    private Stage actual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        cmbCargar.getItems().addAll("Personal", "Emergencia", "Laboral");
        cmbtipoCarga.getItems().addAll("Cónyuge", "Hijo/a");
    }

    public void cargarDatos(Empleado emp, Stage actual) {
        this.emp = emp;
        this.actual = actual;
    }

    @FXML
    private void guardarDatos() {
        this.telefonos = txtNumero.getText();
        if (txtCI.getText().equals("") || txtNombre.getText().equals("") || txtApellidos.getText().equals("")
                || txtNumero.getText().equals("") || cmbCargar.getValue() == null) {
            util.Util.mostrarDialogAlert("Llene todos los campos antes de continuar");
        } else if (!util.Util.verificarPatron("^[0-9]{10}$", txtCI.getText())) {
            util.Util.mostrarDialogAlert("La cédula no tiene el formato correcto");
            txtCI.requestFocus();
        } else if (!util.Util.verificarPatron(".{1,150}", txtNombre.getText())) {
            util.Util.mostrarDialogAlert("El nombre no tiene el formato correcto, no puede tener más de 150 caracteres");
            txtNombre.requestFocus();
        } else if (!util.Util.verificarPatron(".{1,150}", txtApellidos.getText())) {
            util.Util.mostrarDialogAlert("El apellido no tiene el formato correcto, no puede tener más de 150 caracteres");
            txtApellidos.requestFocus();
        } else if (!util.Util.verificarPatron("^((0[1-9])([0-9]{7,8}))$", txtNumero.getText())) {
            util.Util.mostrarDialogAlert("El número no tiene el formato adecuado");
            txtNumero.requestFocus();
        } else if (cmbCargar.getValue().toString().equals("Emergencia")) {
            referencia = new Referencia(this.emp, txtCI.getText(), txtNombre.getText(), txtApellidos.getText(), txtMail.getText(), "E", telefonos);
            try {
                if (referencia.insertar()) {
                    util.Util.mostrarDialog("Se registro la referencia con exito!!");
                    this.emp.getPadre().close();
                    this.emp.fabricarPerfilEmpleado();
                    this.actual.close();
                } else {
                    util.Util.mostrarDialog("No se registro la referencia!!");
                }
            } catch (Exception e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            }
        } else if (cmbCargar.getValue().toString().equals("Personal")) {
            if (dpNacimiento.getValue() == null || txtOcupacion.getText().equals("") || cmbtipoCarga.getValue() == null) {
                util.Util.mostrarDialogAlert("Llene todos los campos antes de continuar");
            } else if (dpNacimiento.getValue().compareTo(LocalDate.now().minusDays(1)) > 0) {
                dpNacimiento.requestFocus();
                util.Util.mostrarDialog("La fecha es errónea");
            } else if (!util.Util.verificarPatron(".{1,50}", txtOcupacion.getText())) {
                util.Util.mostrarDialog("La ocupación no puede superar los 50 caracteres");
                txtOcupacion.requestFocus();
            } else {
                String tc = "";
                if (cmbtipoCarga.getValue().toString().equals("Cónyuge")) {
                    tc = "C";
                } else {
                    tc = "H";
                }
                CargaFamiliar car = new CargaFamiliar(dpNacimiento.getValue(), txtOcupacion.getText(), tc, txtCI.getText(),
                        txtNombre.getText(), txtApellidos.getText(), txtMail.getText(), "", telefonos, this.emp);
                try {
                    if (car.insert()) {
                        util.Util.mostrarDialog("Se registró la referencia con éxito!!");
                        this.emp.getPadre().close();
                        this.emp.fabricarPerfilEmpleado();
                        this.actual.close();
                    } else {
                        util.Util.mostrarDialogAlert("No se registró la referencia");
                    }
                } catch (SQLException ex) {
                    util.Util.mostrarDialogAlert(ex.getMessage());
                }
            }
        } else if (cmbCargar.getValue().toString().equals("Laboral")) {
            if (txtCarga.getText().equals("") || txtEmpresa.getText().equals("")) {
                util.Util.mostrarDialogAlert("Llene todos los campos antes de continuar");
            } else if (!util.Util.verificarPatron(".{1,100}", txtCarga.getText())) {
                util.Util.mostrarDialog("Recuerde que el cargo no puede superar los 100 caracteres");
                txtCarga.requestFocus();
            } else if (!util.Util.verificarPatron(".{1,150}", txtEmpresa.getText())) {
                util.Util.mostrarDialog("Recuerde que el nombre de la empresa no puede superar los 150 caracteres");
                txtEmpresa.requestFocus();
            } else {
                ReferenciaLaboral ref = new ReferenciaLaboral(this.emp, txtCarga.getText(), txtEmpresa.getText(), txtCI.getText(), txtNombre.getText(),
                        txtApellidos.getText(), txtMail.getText(), "", telefonos);
                try {
                    if (ref.insert()) {
                        util.Util.mostrarDialog("Se registró la referencia con éxito!!");
                        this.emp.getPadre().close();
                        this.emp.fabricarPerfilEmpleado();
                        this.actual.close();
                    } else {
                        util.Util.mostrarDialogAlert("No se pudo registrar la referencia laboral");
                    }
                } catch (SQLException ex) {
                    util.Util.mostrarDialogAlert(ex.getMessage());
                }
            }

        }

    }

    @FXML
    private void elegirCarga() {
        if (cmbCargar.getValue().toString().equals("Personal")) {
            PanelLaboral.setVisible(false);
            PanelFamiliar.setVisible(true);
        } else if (cmbCargar.getValue().toString().equals("Laboral")) {
            PanelLaboral.setVisible(true);
            PanelFamiliar.setVisible(false);
        } else {
            PanelLaboral.setVisible(false);
            PanelFamiliar.setVisible(false);
        }

    }

}
