/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import model.Factura;
import model.Proveedores;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevaFactura implements Initializable {

    @FXML
    private DatePicker dpFecha;
    @FXML
    private Button btnFactura;
    private File foto;
    private Proveedores proveedor;
    private Stage padre, hijo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void cargarFactura() {
        foto = util.Util.cargarFoto();
        if (foto != null) {
            btnFactura.setText(foto.getName());
        }
    }

    public void setProveedor(Proveedores proveedor) {
        this.proveedor = proveedor;
    }

    public void setPadre(Stage padre) {
        this.padre = padre;
    }

    public void setHijo(Stage hijo) {
        this.hijo = hijo;
    }

    @FXML
    private void guardarFoto() {
        if (dpFecha.getValue() == null || foto == null) {
            util.Util.mostrarDialog("Llene todos los campos antes de continuar");
        } else if (dpFecha.getValue().compareTo(LocalDate.now()) > 0) {
            util.Util.mostrarDialog("La fecha no puede ser mayor que el día actual");
            dpFecha.requestFocus();
        } else {
            String doc = util.Util.FileToBase64(foto);
            Factura fact = new Factura(doc, dpFecha.getValue(), "", this.proveedor.getRuc(),this.proveedor);
            try {
                if (fact.insertFoto()) {
                    util.Util.mostrarDialog("Se registró la factura con éxito");
                    this.padre.close();
                    this.hijo.close();
                    this.fabricarPerfilProveedor(proveedor);
                } else {
                    util.Util.mostrarDialogAlert("No se puede registrar la factura");
                }
            } catch (SQLException ex) {
                Logger.getLogger(NuevaFactura.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            pcl.setPadre(stage);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

}
