/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Direccion;
import model.Empleado;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevaDireccion implements Initializable {

    @FXML
    private TextField txtProvincia;
    @FXML
    private TextField txtCanton;
    @FXML
    private TextField txtParroquia;
    @FXML
    private TextArea txtDireccion;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnUpdate;
    private Direccion dir;
    private Stage stageActual, padre;
    private Empleado empleado;

    /**
     * Initializes the controller class.
     * Clase que maneja la ventana para agregar una nueva direccion
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarDatosActualizar(Direccion dir, Stage actual) {
        this.dir = dir;
        this.btnSave.setVisible(false);
        this.btnUpdate.setVisible(true);
        txtProvincia.setText(this.dir.getProvincia());
        txtParroquia.setText(this.dir.getParroquia());
        txtDireccion.setText(this.dir.getDescripcion());
        txtCanton.setText(this.dir.getCanton());
        this.stageActual = actual;
    }
    
    public void cargarDatosNuevo(Empleado e, Stage actual, Stage padre){
        this.empleado=e;
        this.stageActual=actual;
        this.btnSave.setVisible(true);
        this.btnUpdate.setVisible(false);
        this.padre = padre;
    }

    @FXML
    private void guardarDireccion() {
        if(validarDatos()){
            if(this.empleado != null){
                this.empleado.setDireccion(new Direccion(this.empleado.getCI(),txtProvincia.getText(), txtCanton.getText(), txtParroquia.getText(), 
                        txtDireccion.getText()));
                try {
                    if(this.empleado.getDireccion().insertarDatos(this.padre)){
                        this.stageActual.close();
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    @FXML
    private void actualizarDireccion() {
        if(validarDatos()){
            try {
                if(this.dir.actualizarDireccion(txtProvincia.getText(), txtCanton.getText(), txtParroquia.getText(), 
                        txtDireccion.getText())){
                    this.stageActual.close();                    
                }
            } catch (SQLException ex) {
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
            
        }

    }

    private boolean validarDatos() {
        if (txtCanton.getText().equals("") || txtDireccion.getText().equals("") || txtParroquia.getText().equals("") || txtProvincia.getText().equals("")) {
            util.Util.mostrarDialog("Llene todos los campos.");
            return false;
        } else if (!util.Util.verificarPatron(".{1,50}", txtProvincia.getText())) {
            txtProvincia.requestFocus();
            return false;
        } else if (!util.Util.verificarPatron(".{1,50}", txtCanton.getText())) {
            txtCanton.requestFocus();
            return false;
        } else if (!util.Util.verificarPatron(".{1,50}", txtParroquia.getText())) {
            txtParroquia.requestFocus();
            return false;
        } else if (!util.Util.verificarPatron(".{1,200}", txtDireccion.getText())) {
            txtDireccion.requestFocus();
            return false;
        }
        return true;
    }

}
