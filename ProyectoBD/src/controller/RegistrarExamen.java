/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import model.Empleado;
import model.Examenes;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class RegistrarExamen implements Initializable {

    @FXML
    private DatePicker dpFecha;
    @FXML
    private TextArea txtObservacion;
    @FXML
    private Button btnFoto;
    @FXML
    private Button btnSave;
    private File foto;
    private Examenes examen;
    private Empleado emp;
    private Stage actual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarDatos(Empleado emp, Stage actual){
        this.emp = emp;
        this.actual = actual;
    }

    @FXML
    private void addFoto() {
        foto = util.Util.cargarFoto();
        if (foto != null) {
            btnFoto.setText(foto.getName());
        }
    }
    
    private void borrarDatos(){
        txtObservacion.clear();
        foto = null;
        dpFecha.setValue(null);
        btnFoto.setText("Cargar Resultado");
        examen = null;
    }

    @FXML
    private void guardarDatos() {
        if (foto == null || txtObservacion.getText().equals("") || dpFecha.getValue() == null) {
            util.Util.mostrarDialog("Debe llenar todos los campos para poder guardar los datos");
        } else if (dpFecha.getValue().compareTo(LocalDate.now()) > 0) {
            util.Util.mostrarDialog("La fecha no puede superar a la fecha actual");
            dpFecha.requestFocus();
        } else if (!util.Util.verificarPatron(".{1,300}", txtObservacion.getText())) {
            util.Util.mostrarDialog("La observación no puede tener más de 300 caracteres");
        } else {
            examen = new Examenes(0,this.emp, util.Util.FileToBase64(foto), txtObservacion.getText(), dpFecha.getValue());
            try {
                if (examen.insertDatos()) {
                    util.Util.mostrarDialog("Se ingresó el resultado al sistema");
                    borrarDatos();  
                    this.emp.getPadre().close();
                    this.emp.fabricarPerfilEmpleado();
                    this.actual.close();
                }
                else {
                    util.Util.mostrarDialogAlert("No se pudo registrar el examen");
                }
            }
            catch(Exception e){
                util.Util.mostrarDialogAlert(e.getMessage());
            }
        }
    }

}
