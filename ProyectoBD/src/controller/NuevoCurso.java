/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Cursos;
import model.Empleado;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoCurso implements Initializable {

    @FXML
    private TextField txtID;
    @FXML
    private TextField txtTema;
    @FXML
    private TextField txtLugar;
    @FXML
    private TextField txtDuracion;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnUpdate;
    private Empleado emp;
    private Stage actual, padre;
    private Cursos c;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    //bandera tru guardar, bandera false update
    public void cargarDatos(Empleado emp, boolean bandera, Stage actual, Stage principal) {
        this.emp = emp;
        this.actual = actual;
        this.padre = principal;
        this.btnSave.setVisible(bandera);
        this.btnUpdate.setVisible(!bandera);
    }

    public void cargarUpdate(Cursos c) {
        this.c = c;
        this.txtID.setText("" + c.getId());
        txtLugar.setText(c.getLugar());
        txtDuracion.setText(c.getDuracion());
        txtTema.setText(c.getTema());
    }

    @FXML
    private void guardarCurso() {
        if (validarDatos()) {
            Cursos c = new Cursos(txtTema.getText(), txtLugar.getText(), txtDuracion.getText(),
                    Integer.parseInt(txtID.getText()), this.emp);
            try {
                if (c.insertar()) {
                    util.Util.mostrarDialog("Se insertó el curso con éxito!!");
                    this.emp.fabricarPerfilEmpleado();
                    this.actual.close();
                    this.padre.close();
                } else {
                    util.Util.mostrarDialog("No se insertó el curso");
                }
            } catch (SQLException ex) {
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        }

    }

    @FXML
    private void actualizarCurso() {
        try {
            if (validarDatos()) {
                if (this.c.actualizar(txtTema.getText(), txtLugar.getText(),
                        txtDuracion.getText(), Integer.parseInt(txtID.getText()))) {
                    util.Util.mostrarDialog("Se actualizó el curso con éxito!!");
                    this.emp.fabricarPerfilEmpleado();
                    this.padre.close();
                    this.actual.close();

                } else {
                    util.Util.mostrarDialog("No se actualizó el curso");
                }
            }
        }
        catch(Exception ex){
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
    }

    private boolean validarDatos() {
        if (txtID.getText().equals("")) {
            util.Util.mostrarDialog("Llene el campo ID para continuar");
            txtID.requestFocus();
            return false;
        } else if (txtTema.getText().length() < 0 || txtTema.getText().length() > 200) {
            util.Util.mostrarDialog("El tema debe tener como máximo 200 caracteres");
            txtTema.requestFocus();
            return false;
        } else if (txtLugar.getText().length() < 0 || txtLugar.getText().length() > 50) {
            util.Util.mostrarDialog("El lugar debe tener como máximo 50 caracteres");
            txtTema.requestFocus();
            return false;
        } else if (txtDuracion.getText().length() < 0 || txtDuracion.getText().length() > 10) {
            util.Util.mostrarDialog("La duración debe tener como máximo 10 caracteres");
            txtDuracion.requestFocus();
            return false;
        }
        return true;
    }

}
