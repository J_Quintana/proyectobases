/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Guardia;
import model.PruebasFisicas;
import model.Usuario;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class VerPruebasFisicas implements Initializable {

    @FXML
    private VBox vbResultados;
    private Guardia g;
    @FXML
    private ImageView btnNuevo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarDatos(Guardia g) {
        if(Usuario.rol == 3){
            btnNuevo.setVisible(false);
        }
        this.g = g;
        try {
            this.g.crearPruebasFisicas();
            if (!this.g.getPruebas().isEmpty()) {
                for (PruebasFisicas pf : this.g.getPruebas()) {
                    this.vbResultados.getChildren().add(pf.dibujar());
                }
            }
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void nuevaPrueba() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaPruebaFisica.fxml"));
            Parent root = fxmlLoader.load();
            NuevaPruebaFisica controller = fxmlLoader.getController();
            controller.cargarDatos(g);
            Scene scene = new Scene(root);
            stage.setTitle("Nueva Prueba Física");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }

    }

}
