/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import model.CargaFamiliar;
import model.Referencia;
import model.ReferenciaLaboral;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class DatosReferencia implements Initializable {

    @FXML
    private TextField txtCedula;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtApellido;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtTelefono;
    @FXML
    private AnchorPane panelFamiliar;
    @FXML
    private TextField txtOcupacion;
    @FXML
    private TextField txtTC;
    @FXML
    private TextField txtNacimiento;
    @FXML
    private AnchorPane panelLaboral;
    @FXML
    private TextField txtCargo;
    @FXML
    private TextField txtEmpresa;
    private Referencia ref;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarReferencia(Referencia ref) {
        this.ref = ref;
        txtCedula.setText(this.ref.getCI());
        txtNombre.setText(this.ref.getNombre());
        txtApellido.setText(this.ref.getApellidos());
        txtEmail.setText(this.ref.getEmail());
        txtTelefono.setText(this.ref.getTelefonos());
        if (this.ref.getTipo().equals("P")) {
            CargaFamiliar car = (CargaFamiliar) this.ref;
            panelFamiliar.setVisible(true);
            txtOcupacion.setText(car.getOcupacion());
            if (car.getTipoC().equals("C")) {
                txtTC.setText("Cónyuge");
            } else {
                txtTC.setText("Hijo/a");
            }
        }
        else if(!this.ref.getTipo().equals("E")){
            ReferenciaLaboral rl = (ReferenciaLaboral) this.ref;
            panelLaboral.setVisible(true);
            txtCargo.setText(rl.getCargo());
            txtEmpresa.setText(rl.getEmpresa());
        }
    }

}
