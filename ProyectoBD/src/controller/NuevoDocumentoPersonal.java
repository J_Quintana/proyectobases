/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.DocumentosPersonales;
import model.Empleado;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoDocumentoPersonal implements Initializable {

    @FXML
    private Button btnDoc;
    @FXML
    private TextField txtID;
    private Empleado emp;
    private File foto;
    private Stage actual;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarDatos(Empleado emp,Stage actual) {
        this.emp = emp;
        this.actual = actual;

    }

    @FXML
    private void cargarDocumento() {
        foto = util.Util.cargarFoto();
        if(foto != null){
            this.btnDoc.setText(foto.getName());
        }
    }

    @FXML
    private void guardarDocumento() {
        if(txtID.getText().equals("") || txtID.getText().length() > 20){
            util.Util.mostrarDialog("El ID no puede tener más de 20 caracteres o estar vacío");
            txtID.requestFocus();
        }
        else if(foto == null)
            util.Util.mostrarDialog("Cargue un documento antes de continuar");
        else {
            String docText = util.Util.FileToBase64(foto);
            if(docText.equals(""))
                util.Util.mostrarDialogAlert("No se pudo procesar. Intente nuevamente");
            else {
                DocumentosPersonales doc = new DocumentosPersonales(docText,txtID.getText(),this.emp);
                try {
                    if(doc.insertar()){
                        util.Util.mostrarDialog("Se guardó el documento con éxito");
                        this.emp.getPadre().close();
                        this.emp.fabricarPerfilEmpleado();
                        this.actual.close();
                    }
                    else {
                        util.Util.mostrarDialog("No se guardó el documento");
                    }
                }
                catch(SQLException ex){
                    util.Util.mostrarDialogAlert(ex.getMessage());
                }
            }
        }
    }

}
