/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Proveedores;
import util.Util;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoProveedor implements Initializable {

    @FXML
    private TextField txtRUC;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtDireccion;
    @FXML
    private Button btnSave;
    private Proveedores proveedor;
    @FXML
    private TextField txtNumero;
    @FXML
    private Button btnAdd;
    @FXML
    private Label lbContador;
    private LinkedList<String> telefonos;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        telefonos = new LinkedList<>();
    }    
    
    private void borrarCajas(){
        txtDireccion.clear();
        txtNombre.clear();
        txtRUC.clear();
        telefonos.clear();
        lbContador.setText("0");
        txtRUC.requestFocus();
    }

    @FXML
    private void guardarDatos() {
        if(txtRUC.getText().equals("") || txtNombre.getText().equals("") || txtDireccion.getText().equals("") || telefonos.isEmpty()){
            Util.mostrarDialog("Llene todos los campos");
        }
        else {
            if(!Util.verificarPatron("^[0-9]{10}001$", txtRUC.getText())){
                Util.mostrarDialog("El RUC no tiene el formato correcto");
                txtRUC.requestFocus();
            }
            else if(!Util.verificarPatron(".{1,100}", txtNombre.getText())){
                Util.mostrarDialog("El nombre no tiene el formato correcto, puede tener máximo 100 caracteres");
                txtNombre.requestFocus();
            }
            else if(!Util.verificarPatron(".{1,150}", txtDireccion.getText())){
                Util.mostrarDialog("La dirección no tiene el formato correcto, puede tener máximo 100 caracteres");
                txtDireccion.requestFocus();
            }
            else {
                this.proveedor = new Proveedores(txtRUC.getText(),txtNombre.getText(),txtDireccion.getText());
                try {
                    proveedor.setTelefonos(telefonos);
                    if(proveedor.insertDatos() && proveedor.insertTelefonos()){
                        util.Util.mostrarDialog("Se ingresó el proveedor con éxito!!");
                        borrarCajas();
                    }
                }
                catch(Exception e){
                    util.Util.mostrarDialogAlert(e.getMessage());
                }
            }
        }
    }

    @FXML
    private void addNumero(ActionEvent event) {
        String tmp = "";
        tmp = txtNumero.getText();
        if(Util.verificarPatron("^((0[1-9])[0-9]{7,8})$", tmp)){
            telefonos.add(tmp);
            txtNumero.setText("");
            lbContador.setText(""+telefonos.size());
        }
        else {
            Util.mostrarDialog("El número ingresado es incorrecto");
            txtNumero.setText("");
            txtNumero.requestFocus();
        }
    }
    
}
