/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Proveedores;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoTelefono implements Initializable {

    @FXML
    private TextField txtNumero;
    private String query;
    private String id;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private Stage stage, stagePadre;
    private boolean bandera=false;//false empleado, true proveedor
    private Object objeto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conex = new Conexion();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setBandera(boolean bandera) {
        this.bandera = bandera;
    }

    public void setObjeto(Object objeto) {
        this.objeto = objeto;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStagePadre(Stage stagePadre) {
        this.stagePadre = stagePadre;
    }

   

    public void cargarDatos(String numero, String query, String id) {
        txtNumero.setText(numero);
        this.query = query;
        this.id = id;
    }

    @FXML
    private void guardarTelefono() throws SQLException {
        if (util.Util.verificarPatron("[0-9]{2}-?[0-9]{7,8}", txtNumero.getText()) && txtNumero.getText().length() <= 10) {
            try {
                con = conex.conectarMySQL();
                ps = con.prepareStatement(this.query);
                ps.setString(2, id);
                ps.setString(1, this.txtNumero.getText());
                int r = ps.executeUpdate();
                if (r > 0) {
                    util.Util.mostrarDialog("Registro exitoso");
                    if(stage !=null)
                        this.stage.close();
                    if(bandera){
                        this.fabricarPerfilProveedor((Proveedores) objeto);
                        if(this.stagePadre != null)
                            this.stagePadre.close();
                        else{
                            System.out.println("Soy nulo no se porqué");
                        }
                    }
                } else {
                    util.Util.mostrarDialog("No se registró el número");
                }
            } catch (SQLException e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            } finally {
                if (con != null) {
                    con.close();
                }
            }
        }
    }
    
    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

}
