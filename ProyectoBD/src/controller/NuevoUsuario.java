/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.Usuario;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoUsuario implements Initializable {

    @FXML
    private TextField txtCI;
    @FXML
    private TextField txtName;
    @FXML
    private PasswordField txtPass;
    @FXML
    private PasswordField txtRepass;
    @FXML
    private ComboBox cmbRol;
    @FXML
    private Button btnFoto;
    @FXML
    private Button btnSave;
    private File foto;
    private Usuario user;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        cmbRol.getItems().addAll("Gerente","Asistente","Contadora");
    }    

    @FXML
    private void addFoto(ActionEvent event) {
        foto = util.Util.cargarFoto();
        
    }
    
    private void borrarDatos(){
        txtCI.clear();
        txtName.clear();
        txtPass.clear();
        txtRepass.clear();
        cmbRol.setValue(null);
        foto=null;
        user=null;
        txtCI.requestFocus();
    }

    @FXML
    private void guardarDatos(ActionEvent event) {
        if(txtCI.getText().equals("") || txtName.getText().equals("") || txtPass.getText().equals("") || 
                txtRepass.getText().equals("") || foto==null || cmbRol.getValue()==null){
            util.Util.mostrarDialog("Llene todos los campos antes de continuar");
        }
        else if(!util.Util.verificarPatron("^[0-9]{10}$", txtCI.getText())){
            util.Util.mostrarDialog("La cédula es inválida");
            txtCI.requestFocus();
        }
        else if(!util.Util.verificarPatron(".{1,100}", txtName.getText())){
            util.Util.mostrarDialog("El nombre no puede exceder los 100 caracteres.");
            txtName.requestFocus();
        }
        else if(!txtPass.getText().equals(txtRepass.getText())){
            util.Util.mostrarDialog("Las contraseñas no coinciden...");
            txtPass.requestFocus();
        }
        else {
            int rol = 0;
            if(cmbRol.getValue().toString().equals("Gerente"))
                rol = 1;
            else if(cmbRol.getValue().toString().equals("Asistente"))
                rol =2;
            else
                rol=3;
            user = new Usuario(txtCI.getText(),util.Util.FileToBase64(foto),txtName.getText(),rol,util.Util.codificarPass(txtPass.getText()));
            try {
                if(user.insertDatos()){
                    util.Util.mostrarDialog("Se ingresó el usuario con éxito!!");
                    borrarDatos();
                }
                else {
                    util.Util.mostrarDialog("No se ingresó el usuario.");
                }
            } catch (SQLException ex) {
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        }
    }
    
}
