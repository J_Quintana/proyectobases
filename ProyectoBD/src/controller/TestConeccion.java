/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class TestConeccion implements Initializable {

    @FXML
    private Button btn1;
    @FXML
    private Button btnInsert;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    @FXML
    private TextField txtcaja;
    @FXML
    private Button btnbuscar;
    private Stage principal;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            conex = new Conexion();
            con = conex.conectarMySQL();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }    

    @FXML
    private void consultarDatos(ActionEvent event) {
        try {
            ps = con.prepareStatement("call inicioSesion('0924995426')");
            res = ps.executeQuery();
            if(res.next()){
                System.out.println(res.getString("nombre") + " / " + res.getString("rol"));
            }
            else {
                System.out.println("Sin datos");
            }
            //con.close();
            
        }
        catch(Exception e ){
            System.out.println(e.getMessage());
        }
    }

    public void setPrincipal(Stage principal) {
        this.principal = principal;
    }

    @FXML
    private void insertar(ActionEvent event) {
        //update es lo mismo que insert solo cambia el query
        // para eldelete se usa lo mismo que para select solo cambia el query
        try {
            ps = con.prepareStatement("insert into empleado (cédula, nombres, apellidos, estado_civil, fecha_de_nacimiento, "
                    + "género, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo)\n" +
                    "values ('0924995426', 'Germán Antonio', 'Cruz Sanchéz', 'Casado', '1966-11-20', 'M',"
                    + " 'guardiagro.gcruz@gmail.com', '2002276363', 'activo', '2002-04-27', 'Gerencia', 'Gerente');");
            // en values se pone ? por cada campo que se va a ingresar para evitar sql injection y 
            //que se pueda insertar desde los componentes
            //ps.setString(1, "cadena"); el indice del valor que se va a ingresar empieza desde 1
            int r = ps.executeUpdate();
            if(r > 0)
                System.out.println("éxito");
            else
                System.out.println("fracaso");
        }
        catch (Exception e ){
            System.out.println(e.getMessage());
        }
        
    }

    @FXML
    private void buscarDatos(ActionEvent event) {
        
        try {
            ps = con.prepareStatement("select * from empleado e join guardia c "
                    + "on e.cédula=c.cédula where e.cédula=?");
            ps.setString(1, txtcaja.getText());
            res = ps.executeQuery();
            if(res.next())
                System.out.println(res.getString("cédula") + " / " + res.getString("cod_guardia"));
            else
                System.out.println("error");
            
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    
}
