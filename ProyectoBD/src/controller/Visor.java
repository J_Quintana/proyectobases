/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.DocumentosPersonales;
import model.Examenes;
import model.Factura;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class Visor implements Initializable {

    private Factura fact;
    //private AnchorPane panelRoot;
    @FXML
    private AnchorPane panelContenido;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void cargarFacturas(Factura fact) {
        this.fact = fact;
        try {
            File path = new File(".\\src\\resources\\tmp\\" + this.fact.getNombre());
            Image imagen = new Image(path.toURI().toString());
            this.dibujarFondo(imagen);
            //imgFactura.setImage(imagen);
            /*imgFactura.prefHeight(imagen.getHeight());
            imgFactura.prefWidth(imagen.getWidth());*/
            /*panelRoot.prefHeight(imagen.getHeight());
            panelRoot.prefWidth(imagen.getWidth());*/

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void cargarCredencial(boolean bandera) {
        if (bandera) {
            try {
                File path = new File(".\\src\\resources\\tmp\\credencialGuardia.jpg");
                Image imagen = new Image(path.toURI().toString());
                this.dibujarFondo(imagen);
                //imgFactura.setImage(imagen);
                /*imgFactura.prefHeight(imagen.getHeight());
                imgFactura.prefWidth(imagen.getWidth());*/
                /*panelRoot.prefHeight(imagen.getHeight());
                panelRoot.prefWidth(imagen.getWidth());*/

            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public void cargarDocumento(DocumentosPersonales doc) {
        try {
            if (util.Util.Base64ToFile(doc.getDocumento(), "doc_personal.png")) {
                File path = new File(".\\src\\resources\\tmp\\doc_personal.png");
                Image imagen = new Image(path.toURI().toString());
                this.dibujarFondo(imagen);
                //imgFactura.setImage(imagen);
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }

    }

    public void cargarExamen(Examenes examen) {
        try {
            if (util.Util.Base64ToFile(examen.getResultado(), "examen_medico.png")) {
                File path = new File(".\\src\\resources\\tmp\\examen_medico.png");
                Image imagen = new Image(path.toURI().toString());
                //imgFactura.setImage(imagen);
                this.dibujarFondo(imagen);
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }

    }

    private void dibujarFondo(Image image) {
        Canvas canvas = new Canvas(800, 600);
        double width = canvas.getWidth();
        double height = canvas.getHeight();

        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(image, 0, 0, width, height, 0, 0, width, height);

        ScrollBar vertical = new ScrollBar();
        vertical.setOrientation(Orientation.VERTICAL);
        vertical.setMin(0);
        vertical.setMax(image.getHeight());
        vertical.setVisibleAmount(height);

        ScrollBar horizontal = new ScrollBar();
        horizontal.setOrientation(Orientation.HORIZONTAL);
        horizontal.setMin(0);
        horizontal.setMax(image.getWidth());
        horizontal.setVisibleAmount(width);

        vertical.valueProperty().addListener((property, old, value) -> {
            double dy = vertical.getValue();
            double dx = horizontal.getValue();

            gc.drawImage(image, dx, dy, width, height, 0, 0, width, height);
        });

        horizontal.valueProperty().addListener((property, old, value) -> {
            double dy = vertical.getValue();
            double dx = horizontal.getValue();

            gc.drawImage(image, dx, dy, width, height, 0, 0, width, height);
        });

        HBox root = new HBox(new VBox(canvas, horizontal), vertical);
        this.panelContenido.getChildren().add(root);

    }
}
