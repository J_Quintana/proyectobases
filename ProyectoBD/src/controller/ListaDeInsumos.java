/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import model.Guardia;
import model.Insumos;
import model.Usuario;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class ListaDeInsumos implements Initializable {

    private Guardia gua;
    @FXML
    private TextField txtID;
    @FXML
    private VBox vbInsumos;
    @FXML
    private ImageView btnNuevo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    /**
     * metodo que carga los datos del guardia
     * @param Guadia 
     */
    public void cargarDatos(Guardia e) {
        if(Usuario.rol == 3){
            btnNuevo.setVisible(false);
        }
        this.gua=e;
        if(!this.gua.getInsumos().isEmpty()){
            for(Insumos i : this.gua.getInsumos()){
                vbInsumos.getChildren().add(i.dibujar());
            }
        }

    }

    @FXML
    private void agregarInsumo() {
        if(txtID.getText().equals("")){
            util.Util.mostrarDialog("Llene el campo ID antes de continuar");
            txtID.requestFocus();
        }
        else {
            try {
                Insumos insumo = new Insumos("","",Integer.parseInt(txtID.getText()),this.gua);
                if(insumo.LigarInsumo()){
                    util.Util.mostrarDialog("Se registró el insumo con éxito");
                    if(this.gua.crearInsumos()){
                        this.gua.getVentanaInsumos().close();
                        insumo.fabricarListaDeInsumos();
                    }
                }
            }
            catch(Exception ex){
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        }

    }

}
