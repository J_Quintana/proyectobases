/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Cursos;
import model.DocumentosPersonales;
import model.Empleado;
import model.Examenes;
import model.Guardia;
import model.Permisos;
import model.Referencia;
import model.Telefono;
import model.Usuario;
import model.Vacaciones;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class VerPerfilEmpleado implements Initializable {

    @FXML
    private Label lbNombre;
    @FXML
    private Label lbCedula;
    @FXML
    private Label lbEstadoC;
    @FXML
    private Label lbNacimiento;
    @FXML
    private Label lbGenero;
    @FXML
    private Label lbEstado;
    @FXML
    private Label lbMail;
    @FXML
    private Label lbSeguro;
    @FXML
    private Label lbIngreso;
    @FXML
    private Label lbDepa;
    @FXML
    private Label lbCargo;
    private Empleado empleado;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    @FXML
    private ImageView imgUser;
    @FXML
    private Label lbRUC;
    @FXML
    private Label lbHorario;
    @FXML
    private Label lbCodigG;
    @FXML
    private AnchorPane panelEmpleado;
    @FXML
    private Button btnCredencial;
    @FXML
    private VBox vbTelefonos;
    private Stage padre;
    @FXML
    private Label lbProv;
    @FXML
    private Label lbCanton;
    @FXML
    private Label lbParro;
    @FXML
    private TextArea txtDir;
    @FXML
    private Button btnNuevaDireccion;
    @FXML
    private Button btnUpdateDireccion;
    @FXML
    private Button btnDeleteDireccion;
    @FXML
    private VBox vbReferencias;
    @FXML
    private VBox vbVacaciones;
    @FXML
    private VBox vbPermisos;
    @FXML
    private VBox vbCursos;
    @FXML
    private Button btnInsumos;
    @FXML
    private Button btnPruebasFisicas;

    @FXML
    private HBox hbDocumentos;
    @FXML
    private HBox hbExamenes;
    private boolean banderaCredencial = false;
    @FXML
    private AnchorPane panelDatos;
    private boolean banderaUpdate = true;
    @FXML
    private Button btnAddTel;
    @FXML
    private Button btnAddRefe;
    @FXML
    private Button btnAddVaca;
    @FXML
    private Button btnAddPerm;
    @FXML
    private Button btnAddCurso;
    @FXML
    private Button btnAddDoc;
    @FXML
    private Button btnAddExa;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conex = new Conexion();
    }
    
    private void aplicarPermisos(){
        if(Usuario.rol != 1){
            btnAddTel.setVisible(false);
            btnAddExa.setVisible(false);
            btnAddDoc.setVisible(false);
            btnAddCurso.setVisible(false);
            btnAddPerm.setVisible(false);
            btnAddVaca.setVisible(false);
            btnAddRefe.setVisible(false);
            btnNuevaDireccion.setVisible(false);
            btnUpdateDireccion.setVisible(false);
            btnDeleteDireccion.setVisible(false);
        }
        if(Usuario.rol == 2)
            btnAddExa.setVisible(true);
    }
    
    public void cargarDatos(Empleado e, boolean bandera, Stage stage) {
        this.padre = stage;
        if (bandera) {
            Guardia g = (Guardia) e;
            panelEmpleado.setVisible(bandera);
            lbHorario.setText(g.getHorario());
            lbRUC.setText(g.getRuc());
            lbCodigG.setText("" + g.getCodGuardia());
            btnCredencial.setVisible(true);
            try {
                if (!g.getCredencial().equals("")) {
                    util.Util.Base64ToFile(g.getCredencial(), "credencialGuardia.jpg");
                    this.banderaCredencial = true;
                } else {
                    System.out.println("soy null credencial");
                }
            } catch (Exception ex) {
                System.out.println("exepcion credencial del guardia en blanco");
                System.out.println(ex.getMessage());
            }
        }
        this.empleado = e;
        crearPerfilEmpleado(this.empleado);
        cargarFotoDePerfil();
        dibujarNumeros();
        try {
            if (this.empleado.crearDireccion()) {
                this.CargarDireccion();
                this.empleado.getDireccion().setStagePadre(this.padre);
                btnNuevaDireccion.setVisible(false);
            } else {
                btnDeleteDireccion.setVisible(false);
                btnUpdateDireccion.setVisible(false);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        crearVacaciones();
        crearPermisos();
        crearCursos();
        crearDocumentos();
        crearExamenes();
        crearReferencias();
        aplicarPermisos();
    }

    private void crearReferencias() {
        vbReferencias.setSpacing(5);
        try {
            if (this.empleado.cargarReferencias()) {
                for (Referencia r : this.empleado.getReferencias()) {
                    this.vbReferencias.getChildren().add(r.dibujar());
                }
            }
        } catch (Exception ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
    }

    private void crearPermisos() {
        this.vbPermisos.setSpacing(5);
        try {
            if (this.empleado.cargarPermisos()) {
                for (Permisos p : this.empleado.getPermisos()) {
                    this.vbPermisos.getChildren().add(p.dibujar());
                }
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private void crearVacaciones() {
        this.vbVacaciones.setSpacing(15);
        try {
            if (this.empleado.crearVacaciones()) {
                for (Vacaciones v : this.empleado.getVacaciones()) {
                    v.setPadre(padre);
                    this.vbVacaciones.getChildren().add(v.dibujar());
                }
            } else {
                System.out.println("No hay vacaciones");
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private String obtenerDepartamento(String codigo) throws SQLException {
        String salida = "";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select descripcion from departamento where id=?");
            ps.setString(1, codigo);
            res = ps.executeQuery();
            if (res.next()) {
                salida = res.getString("descripcion");
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return salida;
    }

    private void crearPerfilEmpleado(Empleado e) {
        if (!e.isEstado()) {
            this.panelDatos.setDisable(true);
            this.panelEmpleado.setDisable(true);
            banderaUpdate = false;
        }
        this.lbNombre.setText(e.getNombres());
        this.lbCedula.setText(e.getCI());
        this.lbEstadoC.setText(e.getEstadoCivil());
        this.lbNacimiento.setText(e.getNacimiento().toString());
        this.lbGenero.setText((e.isGenero()) ? "Masculino" : "Femenino");
        this.lbEstado.setText((e.isEstado()) ? "Activo" : "Inactivo");
        this.lbMail.setText(e.getMail());
        this.lbSeguro.setText(e.getSeguro());
        this.lbIngreso.setText(e.getIngreso().toString());
        String depa = "";
        try {
            depa = obtenerDepartamento(e.getCodDepa());
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        if (depa.equals("")) {
            this.lbDepa.setText("Sin datos");
        } else {
            this.lbDepa.setText(depa);
        }
        this.lbCargo.setText(e.getCargo());
    }

    private void cargarFotoDePerfil() {
        try {
            if (util.Util.Base64ToFile(this.empleado.getFoto(), "fotoperfilempleado.jpg")) {
                File path = new File(".\\src\\resources\\tmp\\fotoperfilempleado.jpg");
                Image imagen = new Image(path.toURI().toString());
                this.imgUser.setImage(imagen);
            }
        } catch (Exception ex) {
            System.out.println("excepción de foto de perfil");
            System.out.println(ex.getMessage());
        }
    }

    private void generarVisor() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVisor.fxml"));
            Parent root = fxmlLoader.load();
            Visor pcl = fxmlLoader.getController();
            pcl.cargarCredencial(this.banderaCredencial);
            Scene scene = new Scene(root);
            stage.setTitle("Credencial");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }

    }

    @FXML
    private void VerCredencial() {
        this.generarVisor();
    }

    private void dibujarNumeros() {
        for (String numero : this.empleado.getTelefonos()) {
            Telefono tel = new Telefono(numero, this.empleado.getCI(), this.padre);
            HBox tmp = tel.dibujarNumero();
            if (tmp != null) {
                this.vbTelefonos.getChildren().add(tmp);
            }
        }
    }

    @FXML
    private void nuevoTelefono() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoNumeroEmpleado.fxml"));
            Parent root = fxmlLoader.load();
            NuevoNumeroEmpleado controlador = fxmlLoader.getController();
            String query = "insert into telefonos values(?,?)";
            controlador.cargarDatosNew(this.padre, stage, query, true, this.empleado.getCI());
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Número");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void CargarDireccion() {
        this.lbProv.setText(this.empleado.getDireccion().getProvincia());
        this.lbCanton.setText(this.empleado.getDireccion().getCanton());
        this.lbParro.setText(this.empleado.getDireccion().getParroquia());
        txtDir.setText(this.empleado.getDireccion().getDescripcion());
    }

    @FXML
    private void nuevaDireccion() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaDireccion.fxml"));
            Parent root = fxmlLoader.load();
            NuevaDireccion controlador = fxmlLoader.getController();
            controlador.cargarDatosNuevo(empleado, stage, this.padre);
            Scene scene = new Scene(root);
            stage.setTitle("Nueva Dirección");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void actualizarDireccion() {
        this.empleado.getDireccion().fabricarVistaNumeroUpdate();
    }

    @FXML
    private void eliminarDireccion() {
        try {
            this.empleado.getDireccion().deleteDireccion();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void NuevaReferencia() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaReferencia.fxml"));
            Parent root = fxmlLoader.load();
            NuevaReferencia controlador = fxmlLoader.getController();
            controlador.cargarDatos(empleado, stage);
            Scene scene = new Scene(root);
            stage.setTitle("Nueva Referencia");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void nuevaVacacion() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaVacacion.fxml"));
            Parent root = fxmlLoader.load();
            NuevaVacacion controlador = fxmlLoader.getController();
            controlador.cargarDatos(empleado, stage, this.padre, true);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevas Vacaciones");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void nuevoPermiso() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoPermiso.fxml"));
            Parent root = fxmlLoader.load();
            NuevoPermiso controlador = fxmlLoader.getController();
            controlador.cargarDatos(this.empleado, stage, this.padre);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Permiso");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void nuevoCurso() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoCurso.fxml"));
            Parent root = fxmlLoader.load();
            NuevoCurso controlador = fxmlLoader.getController();
            controlador.cargarDatos(this.empleado, true, stage, this.padre);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Permiso");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @FXML
    private void verInsumos() {
        try {
            Guardia g = (Guardia) this.empleado;
            g.crearInsumos();
            try {
                Stage stage = new Stage();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/FXMLListaDeInsumos.fxml"));
                Parent root = fxmlLoader.load();
                ListaDeInsumos controller = fxmlLoader.getController();
                g.setVentanaInsumos(stage);
                controller.cargarDatos(g);
                Scene scene = new Scene(root);
                stage.setTitle("Lista de insumos");
                stage.setScene(scene);
                stage.setResizable(false);
                stage.show();
            } catch (IOException i) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", i);
            }
        } catch (Exception e) {
            System.out.println("exepcion insumos");
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    @FXML
    private void verPruebasFisicas() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPruebasFisicas.fxml"));
            Parent root = fxmlLoader.load();
            VerPruebasFisicas controller = fxmlLoader.getController();
            controller.cargarDatos((Guardia) this.empleado);
            Scene scene = new Scene(root);
            stage.setTitle("Lista de Pruebas Físicas");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    @FXML
    private void nuevoDocumento() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoDocumentoPersonal.fxml"));
            Parent root = fxmlLoader.load();
            NuevoDocumentoPersonal controlador = fxmlLoader.getController();
            controlador.cargarDatos(empleado, stage);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Documento Personal");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void crearCursos() {
        try {
            if (this.empleado.crearCursos()) {
                for (Cursos c : this.empleado.getCursos()) {
                    this.vbCursos.getChildren().add(c.dibujar());
                }
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private void crearDocumentos() {
        try {
            if (this.empleado.cargarDocumentosPersonales()) {
                for (DocumentosPersonales doc : this.empleado.getDocPersonales()) {
                    this.hbDocumentos.getChildren().add(doc.dibujar());
                }
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
    }

    @FXML
    private void nuevoExamen() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLRegistrarExamen.fxml"));
            Parent root = fxmlLoader.load();
            RegistrarExamen controlador = fxmlLoader.getController();
            controlador.cargarDatos(empleado, stage);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Examen Médico");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void crearExamenes() {
        try {
            if (this.empleado.cargarExamenes()) {
                for (Examenes exa : this.empleado.getExamenes()) {
                    this.hbExamenes.getChildren().add(exa.dibujar());
                }
            }
        } catch (Exception ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
    }

    @FXML
    private void desacticarEmpleado() {
        try {
            if (banderaUpdate) {
                if (util.Util.confirmationDialog("¿Seguro que desea Desactivar al empleado?")) {
                    if (this.empleado.update_desactivar(true)) {
                        util.Util.mostrarDialog("El empleado se desactivó con éxito. Los cambios se verán reflejados cuando actualice el sistema");
                    } else {
                        util.Util.mostrarDialog("No se pudo desactivar el empleado");
                    }
                }
            }
            else {
                if(util.Util.confirmationDialog("¿Seguro que desea Activar al empleado?")){
                if(this.empleado.update_desactivar(false)){
                    util.Util.mostrarDialog("El empleado se activó con éxito. Los cambios se verán reflejados cuando actualice el sistema");
                }
                else {
                    util.Util.mostrarDialog("No se pudo activar el empleado");
                }
            }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void eliminarEmpleado() {
        try {
            if (util.Util.confirmationDialog("¿Seguro que desea eliminar al empleado?")) {
                if (this.empleado.eliminar()) {
                    util.Util.mostrarDialog("El empleado se eliminó con éxito. Los cambios se verán reflejados cuando actualice el sistema");
                } else {
                    util.Util.mostrarDialog("No se pudo eliminar el empleado");
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}
