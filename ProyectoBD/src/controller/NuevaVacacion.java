/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import model.Empleado;
import model.Vacaciones;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevaVacacion implements Initializable {

    @FXML
    private Button btnSave;
    @FXML
    private Button btnUpdate;
    private Empleado empleado;
    private Stage padre, actual;
    @FXML
    private DatePicker dpInicio;
    @FXML
    private DatePicker dpFin;
    private Vacaciones v;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    // true nuevo, false update
    public void cargarDatos(Empleado e, Stage actual, Stage padre, boolean bandera) {
        this.empleado = e;
        this.actual = actual;
        this.padre = padre;
        btnUpdate.setVisible(!bandera);
        btnSave.setVisible(bandera);
    }

    public void cargarVistaUpdate(Vacaciones v) {
        this.v = v;
        this.dpInicio.setValue(v.getInicio());
        this.dpFin.setValue(v.getFin());
    }

    @FXML
    private void GuardarVacaciones() {
        if (verificarDatos()) {
            Vacaciones v = new Vacaciones(dpInicio.getValue(), dpFin.getValue(), this.empleado, 0);
            try {
                if (v.addVacacion()) {
                    this.actual.close();
                    this.empleado.crearVacaciones();
                    this.empleado.fabricarPerfilEmpleado();
                    this.padre.close();
                }
            } catch (SQLException e) {
                util.Util.mostrarDialog(e.getMessage());
            }
        }
    }

    @FXML
    private void updateVacaciones() {
        if(this.verificarDatos()){
            try {
                if(v.updateDatos(dpInicio.getValue(), dpFin.getValue())){
                    this.empleado.crearVacaciones();
                    this.empleado.fabricarPerfilEmpleado();
                    this.padre.close();
                    this.actual.close();
                }
            } catch (SQLException ex) {
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
            
        }
    }

    private boolean verificarDatos() {
        if (dpInicio.getValue() == null || dpFin.getValue() == null) {
            util.Util.mostrarDialog("LLene todos los campos para poder continuar");
            return false;
        } else if (dpInicio.getValue().compareTo(LocalDate.now()) > 0) {
            util.Util.mostrarDialog("La fecha de inicio no puede ser mayor al día actual!!");
            dpInicio.requestFocus();
            return false;
        } else if ((dpFin.getValue().compareTo(dpInicio.getValue()) <= 0)
                || dpFin.getValue().compareTo(dpInicio.getValue().plusDays(61)) > 0) {
            util.Util.mostrarDialog("La fecha de Fin no puede ser mayor a 60 días de la actual actual!!");
            dpFin.requestFocus();
            return false;
        }
        return true;
    }

}
