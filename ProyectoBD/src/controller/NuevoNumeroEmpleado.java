/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Empleado;
import model.Guardia;
import model.Proveedores;
import model.Telefono;

/**
 * FXML Controller class
 *
 * @author JONATHAN
 */
public class NuevoNumeroEmpleado implements Initializable {

    @FXML
    private TextField txtNumero;
    private Telefono telefono;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    private Stage padre, hijo;
    private String query;
    private boolean bandera = false;
    private String cedula;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        conex = new Conexion();
    }

    public void cargarDatosUpdate(Telefono e, Stage padre, Stage hijo, String query) {
        this.query = query;
        this.padre = padre;
        this.hijo = hijo;
        this.telefono = e;
        txtNumero.setText(e.getNumero());
        this.cedula = e.getCedula();
    }

    public void cargarDatosNew(Stage padre, Stage hijo, String query, boolean bandera, String cedula) {
        this.cedula = cedula;
        this.bandera = bandera;
        this.padre = padre;
        this.hijo = hijo;
        this.query = query;
    }

    @FXML
    private void guardarNumero() {
        if (util.Util.verificarPatron("[0-9]{2}-?[0-9]{7,8}", txtNumero.getText()) && txtNumero.getText().length() <= 10) {
            try {
                con = conex.conectarMySQL();
                ps = con.prepareStatement(this.query);
                if (this.bandera) {
                     ps.setString(1, txtNumero.getText());
                     ps.setString(2, this.cedula);
                } else {
                    ps.setString(1, txtNumero.getText());
                    ps.setString(2, this.telefono.getNumero());
                    ps.setString(3, this.telefono.getCedula());
                }
                int r = ps.executeUpdate();
                if (r > 0) {
                    util.Util.mostrarDialog("Registro exitoso");
                    if (padre != null && hijo != null) {
                        this.fabricarPerfilEmpleado(this.cedula);
                        padre.close();
                        hijo.close();
                    }
                } else {
                    util.Util.mostrarDialog("No se registró el número");
                }
            } catch (SQLException e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }
    }

    private void fabricarPerfilEmpleado(String ci) {
        Empleado emp;
        boolean bandera = true;
        try {
            if (esGuardia(ci)) {
                emp = crearGuardia(ci);
            } else {
                emp = crearEmpleado(ci);
                bandera = false;
            }
            if (emp != null) {
                Stage stage = new Stage();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilEmpleado.fxml"));
                Parent root = fxmlLoader.load();
                VerPerfilEmpleado pe = fxmlLoader.getController();
                pe.cargarDatos(emp, bandera, stage);
                Scene scene = new Scene(root);
                stage.setTitle("Datos del Empleado: " + emp.getNombres());
                stage.setScene(scene);
                stage.setResizable(false);
                stage.show();
            } else {
                System.out.println("Empleado null");
            }

        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private Boolean esGuardia(String ci) {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            return res.next();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    private LinkedList<String> cargarTelefonos(String ci) {
        LinkedList<String> telefonos = new LinkedList<>();
        Connection con2 = null;
        PreparedStatement ps2;
        ResultSet res2;
        try {
            con2 = conex.conectarMySQL();
            ps2 = con2.prepareStatement("select numero from telefonos where cedula=?");
            ps2.setString(1, ci);
            res2 = ps2.executeQuery();
            while (res2.next()) {
                telefonos.add(res2.getString("numero"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (con2 != null) {
                    con2.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return telefonos;
    }

    private Guardia crearGuardia(String ci) throws SQLException {
        Guardia g = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_perfil_guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                g = new Guardia(res.getInt("cod_guardia"), res.getString("credencial"), res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
                g.setHorario(res.getString("horario"));
                g.setRuc(res.getString("RUC"));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return g;
    }

    private Empleado crearEmpleado(String ci) {
        Empleado emp = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from empleado where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                emp = new Empleado(res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        return emp;
    }

}
