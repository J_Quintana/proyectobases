/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Usuario;

/**
 *  Clase que maneja la ventana de login 
 * @author Diego Rojas
 */
public class Login implements Initializable {

    @FXML
    private TextField txtCedula;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    private Stage principal;
    private Usuario user;
    @FXML
    private Button btnIngresar;
    @FXML
    private PasswordField txtPass;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void setPrincipal(Stage principal) {
        this.principal = principal;
    }

    @FXML
    /**
     * metodo que realiza la conxecion y llama al storeProedure inicio Sesion
     */
    public void accionIngresar() {
        try {
            conex = new Conexion();
            con = conex.conectarMySQL();
            ps = con.prepareStatement("call inicioSesion(?)");
            ps.setString(1, txtCedula.getText());
            res = ps.executeQuery();
            if (res.next()) {
                user = new Usuario(res.getString("cedula"), res.getString("foto"), res.getString("nombre"), res.getInt("rol"), res.getString("pass"));
                con.close();
                if (user.getPass().equals(util.Util.codificarPass(txtPass.getText()))) {
                    this.nuevaVentana();
                    txtCedula.clear();
                    txtPass.clear();
                    principal.hide();
                } else {
                    util.Util.mostrarDialogAlert("La contraseña no coincide");
                    txtPass.clear();
                    txtPass.requestFocus();
                }
            } else {
                util.Util.mostrarDialogAlert("El usuario no existe.");
                txtCedula.requestFocus();
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }

    }

    //
    private void nuevaVentana() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLMenuPrincipal.fxml"));
            Parent root = fxmlLoader.load();
            MenuPrincipal menu = fxmlLoader.getController();
            menu.setUser(this.user);
            menu.setPrincipal(principal);
            menu.setActual(stage);
            menu.cargarDatos();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("Menú Principal");
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

}
