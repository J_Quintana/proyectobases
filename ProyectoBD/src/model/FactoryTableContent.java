/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Clase para facilitar el ir agregando datos en los table view del menu principal
 * @author JONATHAN
 */
public class FactoryTableContent {
    private String nombre;
    private String apellido;
    private SimpleStringProperty  ci;
    private SimpleStringProperty direccion;

    public FactoryTableContent(String nombre, String apellido, String ci, String direccion) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ci = new SimpleStringProperty(ci);
        this.direccion = new SimpleStringProperty(direccion);
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public SimpleStringProperty getCi() {
        return ci;
    }

    public SimpleStringProperty getDireccion() {
        return direccion;
    }
    
    public SimpleStringProperty getName(){
        return new SimpleStringProperty(this.nombre+" "+this.apellido);
    }
    
}
