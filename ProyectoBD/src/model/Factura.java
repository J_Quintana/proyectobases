/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.VerPerfilProveedor;
import controller.Visor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Factura {

    private final String documento;
    private final LocalDate fecha;
    private final String id;
    private final String ruc;
    private final String nombre;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private final Proveedores proveedor;
    private Stage padre;

    public Factura(String documento, LocalDate fecha, String id, String ruc, Proveedores proveedor) {
        this.documento = documento;
        this.fecha = fecha;
        this.id = id;
        this.ruc = ruc;
        nombre = "factura_" + this.ruc + "_" + this.id + ".png";
        conex = new Conexion();
        this.proveedor = proveedor;
    }

    public String getDocumento() {
        return documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setPadre(Stage padre) {
        this.padre = padre;
    }

    public boolean insertFoto() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into facturas(documento,fecha,ruc) values(?,?,?)");
            ps.setString(1, this.documento);
            Date dat = Date.from(fecha.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(2, new java.sql.Date(dat.getTime()));
            ps.setString(3, this.ruc);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    private boolean deleteFoto(){
        try {
            String query = "delete from facturas where id=?";
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, id);
            if( ps.executeUpdate() >0)
                return true;
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public VBox dibujarFactura() {
        VBox root = new VBox(10);
        try {
            if (!this.documento.equals("") && util.Util.Base64ToFile(documento, this.nombre)) {
                ImageView img = new ImageView(new Image("/resources/botones/factura.png"));
                img.setFitWidth(75);
                img.setFitHeight(75);
                root.getChildren().add(img);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        Label lbFecha = new Label(this.fecha.toString());
        root.getChildren().addAll(lbFecha, this.btnSee(),this.btnDelete());
        root.setAlignment(Pos.CENTER);
        return root;
    }

    private ImageView btnSee() {
        ImageView btn = null;
        try {
            btn = new ImageView(new Image("/resources/botones/see.png"));
            btn.setFitWidth(25);
            btn.setFitHeight(25);
            btn.setOnMousePressed(e -> {
                this.generarVisor();
            });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return btn;
    }

    private void generarVisor() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVisor.fxml"));
            Parent root = fxmlLoader.load();
            Visor pcl = fxmlLoader.getController();
            if (!this.documento.equals("")) {
                pcl.cargarFacturas(this);
            }
            Scene scene = new Scene(root);
            stage.setTitle("Factura: " + nombre);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }

    }

    private ImageView btnDelete() {
        ImageView btn = null;
        try {
            btn = new ImageView(new Image("/resources/botones/delete.png"));
            btn.setFitWidth(25);
            btn.setFitHeight(25);
            btn.setOnMousePressed(e -> {
                if(this.deleteFoto()){
                    util.Util.mostrarDialog("Se elimino la factura con exito");
                    this.fabricarPerfilProveedor(this.proveedor);
                    if(this.padre != null)
                        this.padre.close();
                }
                else {
                    util.Util.mostrarDialogAlert("No se pudo eliminar la factura");
                }
            });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return btn;
    }
    
    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

}
