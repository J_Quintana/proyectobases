/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.DatosReferencia;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Referencia {

    private String CI;
    private String nombre;
    private String apellidos;
    private String email;
    private String tipo;
    private String telefonos;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private Empleado empleado;

    public Referencia(Empleado e, String CI, String nombre, String apellidos, String email, String tipo, String telefonos) {
        this.CI = CI;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.tipo = tipo;
        this.telefonos = telefonos;
        this.empleado = e;
        conex = new Conexion();
    }

    public String getCI() {
        return CI;
    }

    private boolean insertDatos() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into referencia values(?,?,?,?,?,?,?)");
            ps.setString(1, this.CI);
            ps.setString(2, this.nombre);
            ps.setString(3, this.apellidos);
            ps.setString(4, this.email);
            ps.setString(5, this.tipo);
            ps.setString(6, this.empleado.getCI());
            ps.setString(7, this.telefonos);
            int r = ps.executeUpdate();
            con.close();
            if (r > 0) {
                return true;
            }

        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean insertar() {
        try {
            if (insertDatos()) {
                return true;
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        return false;
    }

    public boolean eliminar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from referencia where cedula=? and cedula_empleado=?");
            ps.setString(1, this.CI);
            ps.setString(2, this.empleado.getCI());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public HBox dibujar() {
        String cssLayout = "-fx-border-color: red;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: dashed;\n";
        HBox root = new HBox(5);
        VBox root2 = new VBox(3);
        Label lbCedula = new Label("Cedula: " + this.CI);
        Label lbNombre = new Label(this.nombre + " " + this.apellidos);
        String tmp = "";
        if (this.tipo.equals("P")) {
            tmp = "Personal";
        } else if (this.tipo.equals("E")) {
            tmp = "Emergencia";
        } else {
            tmp = "Laboral";
        }
        Label lbTipo = new Label("Tipo: " + tmp);
        root2.getChildren().addAll(lbCedula, lbNombre, lbTipo);
        root.getChildren().addAll(root2, btnSee(),btnDelete());
        root.setStyle(cssLayout);
        root.setAlignment(Pos.CENTER);
        return root;
    }

    private ImageView btnSee() {
        ImageView btn = new ImageView(new Image("/resources/botones/see.png"));
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            this.fabricarVista();
        });
        return btn;
    }

    private ImageView btnDelete() {
        ImageView btn = new ImageView(new Image("/resources/botones/delete.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            System.out.println("Eliminando");
            try {
                if(util.Util.confirmationDialog("Seguro que quiere eliminar el registro")){
                    if(this.eliminar()){
                        this.empleado.fabricarPerfilEmpleado();
                        this.empleado.getPadre().close();
                    }
                }
            }
            catch(SQLException ex){
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        });
        return btn;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    private void fabricarVista(){
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLDatosReferencia.fxml"));
            Parent root = fxmlLoader.load();
            DatosReferencia controller = fxmlLoader.getController();
            controller.cargarReferencia(this);
            Scene scene = new Scene(root);
            stage.setTitle("Datos de Referencia ");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }
}
