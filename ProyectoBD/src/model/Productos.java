/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.NuevoProducto;
import controller.VerPerfilProveedor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Productos {

    private final String nombre;
    private final String descripcion;
    private final Proveedores Proveedor;
    private final String id;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private Stage padre;

    public Productos(Stage padre, String nombre, String descripcion, Proveedores proveedor, String id) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.Proveedor = proveedor;
        this.padre = padre;
        conex = new Conexion();
    }

    public HBox dibujarProducto() {
        HBox root = new HBox(10);
        Label lbNombre = new Label(nombre);
        Label lbDescripcion = new Label(descripcion);
        root.getChildren().addAll(lbNombre, lbDescripcion, this.btnEliminar(),this.btnUpdate());
        return root;
    }

    private ImageView btnEliminar() {
        final ImageView btnDelete = new ImageView(new Image("/resources/botones/delete.png"));
        btnDelete.setFitHeight(25);
        btnDelete.setFitWidth(25);
        btnDelete.setOnMousePressed(e -> {
            try {
                if (this.borrarProducto()) {
                    util.Util.mostrarDialog("Se elimino el con exito el producto");
                    this.padre.close();
                    this.fabricarPerfilProveedor(Proveedor);
                } else {
                    util.Util.mostrarDialogAlert("No se pudo eliminar el producto");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Productos.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        return btnDelete;
    }

    private ImageView btnUpdate() {
        final ImageView btnUpdate = new ImageView(new Image("/resources/botones/update.png"));
        btnUpdate.setFitHeight(25);
        btnUpdate.setFitWidth(25);
        btnUpdate.setOnMousePressed(e -> {
            try {
                Stage stage = new Stage();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoProducto.fxml"));
                Parent root = fxmlLoader.load();
                NuevoProducto npro = fxmlLoader.getController();
                npro.setBandera(false);
                npro.setHijo(stage);
                npro.setPadre(padre);
                npro.setId(id);
                npro.setProveedor(Proveedor);
                npro.registrarTextBox(nombre, descripcion);
                Scene scene = new Scene(root);
                stage.setTitle("Datos del producto");
                stage.setScene(scene);
                stage.setResizable(false);
                stage.show();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        });
        return btnUpdate;
    }

    private boolean borrarProducto() throws SQLException {
        String query = "delete from productos where id=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, id);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            pcl.setPadre(stage);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

}
