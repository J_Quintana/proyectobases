/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author JONATHAN
 */
public class ResumeGuardia {
    private String horario;
    private String nombre;
    private String apellidos;
    private String ci;

    public ResumeGuardia(String horario, String nombre, String apellidos, String ci) {
        this.horario = horario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.ci = ci;
    }

    public String getHorario() {
        return horario;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCi() {
        return ci;
    }

    @Override
    public String toString() {
        return "ResumeGuardia{" + "horario=" + horario + ", nombre=" + nombre + ", apellidos=" + apellidos + ", ci=" + ci + '}';
    }
    
    
    
}
