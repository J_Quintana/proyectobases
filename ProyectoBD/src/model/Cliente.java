/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;

/**
 * Clase que modela a un cliente
 * @author JONATHAN
 */
public class Cliente {
    private String RUC;
    private String nombre;
    private String direccion;
    private String telefono;
    private String actPrincipal;
    private String representante;
    private LinkedList<ResumeGuardia> guardias;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;

    public Cliente(String RUC, String nombre, String direccion, String telefono, String actPrincipal, String representante) {
        this.RUC = RUC;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.actPrincipal = actPrincipal;
        this.representante = representante;
        guardias = new LinkedList<>();
        conex = new Conexion();
    }
    
    public boolean addResumeGuardia(ResumeGuardia guardia){
        return this.guardias.add(guardia);
    }

    public LinkedList<ResumeGuardia> getGuardias() {
        return guardias;
    }



    public String getRUC() {
        return RUC;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getActPrincipal() {
        return actPrincipal;
    }

    public String getRepresentante() {
        return representante;
    }
    
    
    public boolean insertDatos() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into cliente values(?,?,?,?,?,?)");
            ps.setString(1, RUC);
            ps.setString(2, nombre);
            ps.setString(3, direccion);
            ps.setString(4, telefono);
            ps.setString(5, actPrincipal);
            ps.setString(6, representante);
            int r = ps.executeUpdate();
            con.close();
            if(r > 0)
                return true;
            
        }
        catch(Exception e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }
    
    public static String querySimple(String ruc){
        return "select * from cliente where ruc='"+ruc+"'" ;
    }
    
    public static String queryResumeGuardia(String ruc){
        return "call datosResumenGuardia('"+ruc+"')";
    }

    @Override
    public String toString() {
        return "Cliente{" + "RUC=" + RUC + ", nombre=" + nombre + ", direccion=" + direccion + ", telefono=" + telefono + ", actPrincipal=" + actPrincipal + ", representante=" + representante + '}';
    }
    
}
