/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.VerPruebasFisicas;
import controller.Visor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class PruebasFisicas {

    private String nombre;
    private String resultado;
    private int id;
    private Guardia g;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;

    public PruebasFisicas(int id, String nombre, String resultado, Guardia g) {
        this.id = id;
        this.nombre = nombre;
        this.resultado = resultado;
        this.g = g;
        conex = new Conexion();
    }

    public boolean insertar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into pruebas_fisicas (nombre,resultado,cod_guardia) values(?,?,?)");
            ps.setString(1, this.nombre);
            ps.setString(2, this.resultado);
            ps.setInt(3, this.g.getCodGuardia());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public HBox dibujar() {
        HBox root = new HBox(5);
        root.setAlignment(Pos.CENTER);
        try {
            ImageView btnDelete = new ImageView(new Image("/resources/botones/delete-black.png"));
            btnDelete.setFitWidth(25);
            btnDelete.setFitHeight(25);
            TextArea lbnombre = new TextArea(this.nombre);
            lbnombre.setWrapText(true);
            lbnombre.setPrefSize(100, 100);
            lbnombre.setEditable(false);
            btnDelete.setOnMousePressed(e -> {
                try {
                    if(util.Util.confirmationDialog("Seguro que desea eliminar")){
                        if(this.eliminar()){
                            util.Util.mostrarDialog("Se elimino la prueba con exito, para ver los cambios actualice la ventana");
                        }
                        else{
                            util.Util.mostrarDialog("No se pudo eliminar la prueba fisica");
                        }
                    }
                }
                catch(SQLException ex){
                    util.Util.mostrarDialogAlert(ex.getMessage());
                }
            });
            root.getChildren().addAll(lbnombre, btnDelete);
        } catch (Exception ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        return root;
    }

    public String getResultado() {
        return resultado;
    }
    
    private boolean eliminar() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from pruebas_fisicas where id=?");
            ps.setInt(1,id);
            if(ps.executeUpdate() > 0)
                return true;
        }
        catch(SQLException ex){
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }

}
