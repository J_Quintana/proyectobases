/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.ListaDeInsumos;
import controller.VerPerfilEmpleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Insumos {

    private String nombre;
    private String descripcion;
    private int id_insumo;
    private Guardia gua;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;

    public Insumos(String nombre, String descripcion, int id_insumo, Guardia gua) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.id_insumo = id_insumo;
        this.gua = gua;
        conex = new Conexion();
    }

    public boolean LigarInsumo() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into insumos(cod_guardia,id_producto) values(?,?)");
            ps.setInt(1, this.gua.getCodGuardia());
            ps.setInt(2,this.id_insumo);
            if(ps.executeUpdate() > 0)
                return true;
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
            util.Util.mostrarDialogAlert("El codigo del producto no es valido");
        }
        finally{
            if(con != null)
                con.close();
        }
        return false;
    }

    public HBox dibujar() {
        HBox root = new HBox(3);
        Label lbNombre = new Label(this.nombre);
        TextArea taDescripcion = new TextArea("Descripción: " +this.descripcion);
        taDescripcion.setPrefSize(150, 150);
        taDescripcion.setWrapText(true);
        VBox root2 = new VBox(5);
        root2.setAlignment(Pos.CENTER);
        root.setAlignment(Pos.CENTER);
        root2.getChildren().addAll(lbNombre, taDescripcion);
        root.getChildren().addAll(root2, btnDelete());
        return root;
    }

    private boolean delete() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from insumos where id=?");
            ps.setInt(1, this.id_insumo);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private ImageView btnDelete() {
        ImageView btn = new ImageView(new Image("/resources/botones/delete-black.png"));
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            if (util.Util.confirmationDialog("Seguro que quiere eliminar la vacacion")) {
                try {
                    if (delete()) {
                        util.Util.mostrarDialog("Se elimino el registro con exito");
                        if (this.gua.crearInsumos()) {
                            this.gua.getVentanaInsumos().close();
                            this.fabricarListaDeInsumos();
                        }
                    } else {
                        util.Util.mostrarDialog("No se pudo eliminar el insumo");
                    }
                } catch (SQLException ex) {
                    util.Util.mostrarDialogAlert(ex.getMessage());
                }
            }
        });
        return btn;
    }

    public void fabricarListaDeInsumos() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLListaDeInsumos.fxml"));
            Parent root = fxmlLoader.load();
            ListaDeInsumos controller = fxmlLoader.getController();
            controller.cargarDatos(gua);
            Scene scene = new Scene(root);
            stage.setTitle("Insumos en uso");
            stage.setScene(scene);
            stage.setResizable(false);
            this.gua.setVentanaInsumos(stage);
            stage.show();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

}
