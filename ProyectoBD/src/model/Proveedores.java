/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Proveedores {

    private final String ruc;
    private final String nombre;
    private final String direccion;
    private LinkedList<Factura> facturas;
    private LinkedList<Productos> productos;
    private LinkedList<String> telefonos;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    private LinkedList<Telefono_Proveedores> tel;
    private Stage stage;

    public Proveedores(String ruc, String nombre, String direccion) {
        this.ruc = ruc;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefonos = new LinkedList<>();
        conex = new Conexion();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    
    @Override
    public String toString() {
        return "Proveedores{" + "ruc=" + ruc + ", nombre=" + nombre + ", direccion=" + direccion + '}';
    }

    public boolean addFactura(Factura factura) {
        return this.facturas.add(factura);
    }

    public boolean addProducto(Productos producto) {
        return this.productos.add(producto);
    }

    public void setTelefonos(LinkedList<String> telefonos) {
        this.telefonos = telefonos;
    }
    
    public String queryDeleteTelefono(){
        return "delete from telefono_proveedor where ruc=? and telefono=?";
    }

    public boolean insertTelefonos() throws SQLException {
        try {
            int cont = 0;
            int r = 0;
            con = conex.conectarMySQL();
            for (String s : this.telefonos) {
                ps = con.prepareStatement("insert into telefono_pr0oveedor values(?,?)");
                ps.setString(1, s);
                ps.setString(2, this.ruc);
                r = ps.executeUpdate();
                if (r > 0) {
                    cont++;
                }
            }
            con.close();
            if (cont == telefonos.size()) {
                return true;
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    /**
     * 
     * @return true si se anadio al provedor en la base de datos
     * @throws SQLException 
     */
    public boolean insertDatos() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into proveedores values(?,?,?)");
            ps.setString(1, ruc);
            ps.setString(2, nombre);
            ps.setString(3, direccion);
            int r = ps.executeUpdate();
            con.close();
            if(r>0)
                return true;
        }
        catch(Exception e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally{
            if(con != null)
                con.close();
        }
        return false;
    }
    
    public static String querySimple(String ruc){
        return "select * from proveedores where ruc='"+ruc+"'";
    }

    public String getRuc() {
        return ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }
    
    public boolean tieneTelefonos() throws SQLException{
        String query = "select * from telefono_proveedor where ruc=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, ruc);
            res = ps.executeQuery();
            return res.next();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public boolean graficarTelefonos() throws SQLException{
        tel = new LinkedList<>();
        String query = "select * from telefono_proveedor where ruc=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, ruc);
            res = ps.executeQuery();
            while(res.next()){
                tel.add(new Telefono_Proveedores(res.getString("telefono"),this,this.stage));
            }
            return true;
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public LinkedList<Telefono_Proveedores> getTel() {
        return tel;
    }
    
    public boolean tieneProductos() throws SQLException{
        String query = "select * from productos where ruc=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, ruc);
            res = ps.executeQuery();
            return res.next();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public boolean graficarProductos() throws SQLException{
        productos = new LinkedList<>();
        String query = "select * from productos where ruc=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, ruc);
            res = ps.executeQuery();
            while(res.next()){
                productos.add(new Productos(stage,res.getString("nombre"),res.getString("descripcion"),this,res.getString("id")));
            }
            return true;
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public LinkedList<Productos> getProductos() {
        return productos;
    }
    
    public boolean tieneFacturas() throws SQLException{
        String query = "select * from facturas where ruc=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, ruc);
            res = ps.executeQuery();
            return res.next();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }
    
    public boolean graficarFacturas() throws SQLException{
        facturas = new LinkedList<>();
        String query = "select * from facturas where ruc=?";
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(query);
            ps.setString(1, ruc);
            res = ps.executeQuery();
            Factura tmp=null;
            while(res.next()){
                tmp = new Factura(res.getString("documento"),res.getDate("fecha").toLocalDate(),res.getString("id"),ruc,this);
                tmp.setPadre(stage);
                facturas.add(tmp);
            }
            return true;
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public LinkedList<Factura> getFacturas() {
        return facturas;
    }
    
    public boolean eliminar() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from proveedores where ruc=?");
            ps.setString(1,this.ruc);
            if(ps.executeUpdate() > 0)
                return true;
        }
        catch(Exception ex){
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }
    
    
    
}
