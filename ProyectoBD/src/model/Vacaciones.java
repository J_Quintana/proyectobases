/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.NuevaVacacion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Vacaciones {
    private LocalDate inicio;
    private LocalDate fin;
    private String cedula;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private Empleado ep;
    private Stage padre;
    private int id;

    public Vacaciones(LocalDate inicio, LocalDate fin, Empleado ep, int id) {
        this.id=id;
        this.ep = ep;
        this.inicio = inicio;
        this.fin = fin;
        this.cedula = ep.getCI();
        conex = new Conexion();
    }

    public LocalDate getInicio() {
        return inicio;
    }

    public LocalDate getFin() {
        return fin;
    }
    
    public HBox dibujar(){
        HBox root = new HBox(5);
        VBox rootEtiquetas = new VBox(5);
        Label lInicio = new Label("Fecha de inicio: " + this.inicio.toString());
        Label lFin = new Label("Fecha de fin: " + this.fin.toString());
        rootEtiquetas.getChildren().addAll(lInicio,lFin);
        root.getChildren().addAll(rootEtiquetas,btnUpdate(),btnDelete());
        rootEtiquetas.setAlignment(Pos.CENTER_RIGHT);
        root.setAlignment(Pos.CENTER_LEFT);
        return root;
    }

    public void setPadre(Stage padre) {
        this.padre = padre;
    }

    public boolean updateDatos(LocalDate inicio, LocalDate fin) throws SQLException{
        this.inicio = inicio;
        this.fin = fin;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("update vacaciones set fecha_inicio=?, fecha_fin=? where id=?");
            Date dat = Date.from(this.inicio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(1, new java.sql.Date(dat.getTime()));
            Date dat2 = Date.from(this.fin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(2, new java.sql.Date(dat2.getTime()));
            ps.setInt(3, id);
            if(ps.executeUpdate() > 0)
                return true;
            
        }
        catch(SQLException e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }
        
    private ImageView btnUpdate(){
        ImageView btn = new ImageView(new Image("/resources/botones/update.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            System.out.println("Actualizando");
            this.cargarVistaUpdate();
        });
        return btn;
    }
    
    private ImageView btnDelete(){
        ImageView btn = new ImageView(new Image("/resources/botones/delete.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
           if(util.Util.confirmationDialog("Seguro que quiere eliminar la vacacion")){
               try {
                   if(this.deleteVacacion()){
                       util.Util.mostrarDialog("Se elimino el registro con exito");
                       this.ep.crearVacaciones();
                       this.ep.fabricarPerfilEmpleado();
                       this.padre.close();
                   }
               }
               catch(Exception ex){
                   util.Util.mostrarDialogAlert(ex.getMessage());
               }
           }
        });
        return btn;
    }
    
    public boolean addVacacion() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into vacaciones (fecha_inicio, fecha_fin,cedula) values(?,?,?)");
            Date dat = Date.from(this.inicio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(1, new java.sql.Date(dat.getTime()));
            java.util.Date dat2 = java.util.Date.from(this.fin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(2, new java.sql.Date(dat2.getTime()));
            ps.setString(3, this.cedula);
            if(ps.executeUpdate() > 0)
                return true;
            
        }
        catch(SQLException e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }
    
    private void cargarVistaUpdate(){
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaVacacion.fxml"));
            Parent root = fxmlLoader.load();
            NuevaVacacion controlador = fxmlLoader.getController();
            controlador.cargarDatos(ep,stage,padre,false);
            controlador.cargarVistaUpdate(this);
            Scene scene = new Scene(root);
            stage.setTitle("Nueva Vacación");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    private boolean deleteVacacion() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from vacaciones where id=?");
            ps.setInt(1, id);
            if(ps.executeUpdate() > 0)
                return true;
        }
        catch(Exception e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }
    
    
    
    
}
