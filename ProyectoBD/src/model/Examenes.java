/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.Visor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Examenes {

    private String resultado;
    private String observacion;
    private LocalDate fecha;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private Empleado emp;
    private int ID;

    public Examenes(int ID, Empleado emp, String resultado, String observacion, LocalDate fecha) {
        this.ID = ID;
        this.emp = emp;
        this.resultado = resultado;
        this.observacion = observacion;
        this.fecha = fecha;
        conex = new Conexion();
    }

    public boolean insertDatos() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into examenes (Resultado, observacion,fecha, cedula) values(?,?,?,?)");
            ps.setString(1, resultado);
            ps.setString(2, observacion);
            Date dat = Date.from(fecha.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(3, new java.sql.Date(dat.getTime()));
            ps.setString(4, this.emp.getCI());
            int r = ps.executeUpdate();
            con.close();
            if (r > 0) {
                return true;
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public VBox dibujar() {
        VBox root = new VBox(10);
        Label lbFecha = new Label("Fecha: " + this.fecha.toString());
        TextArea taObservacion = new TextArea("Observacion: " + this.observacion);
        taObservacion.setPrefSize(125, 125);
        taObservacion.setEditable(false);
        taObservacion.setWrapText(true);
        HBox root2 = new HBox(10);
        root2.setAlignment(Pos.CENTER);
        root.setAlignment(Pos.CENTER);
        root2.getChildren().addAll(btnSee(), btnDelete());
        root.getChildren().addAll(lbFecha, taObservacion, root2);

        return root;
    }

    public boolean eliminar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from examenes where id=?");
            ps.setInt(1, ID);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private ImageView btnDelete() {
        ImageView btn = null;
        try {
            btn = new ImageView(new Image("/resources/botones/delete.png"));
            if (Usuario.rol == 3) {
                btn.setVisible(false);
            }
            btn.setFitWidth(25);
            btn.setFitHeight(25);
            btn.setOnMousePressed(e -> {
                try {
                    if (util.Util.confirmationDialog("Seguro de que quiere eliminar")) {
                        if (eliminar()) {
                            util.Util.mostrarDialog("Se elimino con exito el documento");
                            this.emp.getPadre().close();
                            this.emp.fabricarPerfilEmpleado();
                        } else {
                            util.Util.mostrarDialog("No se pudo eliminar el documento");
                        }
                    }
                } catch (SQLException sqlex) {
                    util.Util.mostrarDialogAlert(sqlex.getMessage());
                }
            });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return btn;
    }

    private void generarVisor() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVisor.fxml"));
            Parent root = fxmlLoader.load();
            Visor pcl = fxmlLoader.getController();
            pcl.cargarExamen(this);
            Scene scene = new Scene(root);
            stage.setTitle("Examen medico");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }

    }

    private ImageView btnSee() {
        ImageView btn = null;
        try {
            btn = new ImageView(new Image("/resources/botones/see.png"));
            btn.setFitWidth(25);
            btn.setFitHeight(25);
            btn.setOnMousePressed(e -> {
                this.generarVisor();
            });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return btn;
    }

    // metodos get
    public String getResultado() {
        return resultado;
    }

}
