/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.NuevoNumeroEmpleado;
import controller.VerPerfilEmpleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Telefono {

    private String numero;
    private final String cedula;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private final Stage stagePadre;
    private ResultSet res;

    public Telefono(String numero, String cedula, Stage stage) {
        this.numero = numero;
        this.cedula = cedula;
        this.stagePadre = stage;
        conex = new Conexion();
    }

    public String getNumero() {
        return numero;
    }

    public String getCedula() {
        return cedula;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public HBox dibujarNumero() {
        Label lbNumero = new Label(this.numero);
        final ImageView btnUpdate = new ImageView(new Image("/resources/botones/update.png"));
        final ImageView btnDelete = new ImageView(new Image("/resources/botones/delete.png"));
        btnUpdate.setFitWidth(25);
        btnUpdate.setFitHeight(25);
        btnDelete.setFitWidth(25);
        btnDelete.setFitHeight(25);
        if(Usuario.rol != 1){
            btnDelete.setVisible(false);
            btnUpdate.setVisible(false);
        }
        HBox root = new HBox(10);
        root.getChildren().addAll(lbNumero, btnDelete, btnUpdate);
        btnDelete.setOnMousePressed(e -> {
            if (util.Util.confirmationDialog("Desea eliminar el télefono?")) {
                try {
                    if (this.deleteTelefono()) {
                        util.Util.mostrarDialog("Se elimino con exito el número!!");
                        this.fabricarPerfilEmpleado(this.cedula);
                        if (this.stagePadre != null) {
                            this.stagePadre.close();
                        } else {
                            System.out.println("Soy nulo");
                        }
                    } else {
                        util.Util.mostrarDialogAlert("No se pudo eliminar el numero");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Telefono_Proveedores.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        btnUpdate.setOnMousePressed(e -> {
            this.fabricarVistaNumeroUpdate();
        });
        root.setAlignment(Pos.CENTER);
        return root;
    }

    private boolean deleteTelefono() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from telefonos where numero=? and cedula=?");
            ps.setString(2, this.cedula);
            ps.setString(1, numero);
            int r = ps.executeUpdate();
            if (r > 0) {
                return true;
            }

        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private void fabricarPerfilEmpleado(String ci) {
        Empleado emp;
        boolean bandera = true;
        try {
            if (esGuardia(ci)) {
                emp = crearGuardia(ci);
            } else {
                emp = crearEmpleado(ci);
                bandera = false;
            }
            if (emp != null) {
                Stage stage = new Stage();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilEmpleado.fxml"));
                Parent root = fxmlLoader.load();
                VerPerfilEmpleado pe = fxmlLoader.getController();
                pe.cargarDatos(emp, bandera, stage);
                Scene scene = new Scene(root);
                stage.setTitle("Datos del Empleado: " + emp.getNombres());
                stage.setScene(scene);
                stage.setResizable(false);
                stage.show();
            } else {
                System.out.println("Empleado null");
            }

        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private Boolean esGuardia(String ci) {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            return res.next();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    private LinkedList<String> cargarTelefonos(String ci) {
        LinkedList<String> telefonos = new LinkedList<>();
        Connection con2 = null;
        PreparedStatement ps2;
        ResultSet res2;
        try {
            con2 = conex.conectarMySQL();
            ps2 = con2.prepareStatement("select numero from telefonos where cedula=?");
            ps2.setString(1, ci);
            res2 = ps2.executeQuery();
            while (res2.next()) {
                telefonos.add(res2.getString("numero"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (con2 != null) {
                    con2.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return telefonos;
    }

    private Guardia crearGuardia(String ci) throws SQLException {
        Guardia g = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_perfil_guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                g = new Guardia(res.getInt("cod_guardia"), res.getString("credencial"), res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
                g.setHorario(res.getString("horario"));
                g.setRuc(res.getString("RUC"));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return g;
    }

    private Empleado crearEmpleado(String ci) {
        Empleado emp = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from empleado where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                emp = new Empleado(res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        return emp;
    }

    private void fabricarVistaNumeroUpdate() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoNumeroEmpleado.fxml"));
            Parent root = fxmlLoader.load();
            NuevoNumeroEmpleado controlador = fxmlLoader.getController();
            String query = "update telefonos set numero=? where numero=? and cedula=?";
            controlador.cargarDatosUpdate(this, this.stagePadre, stage, query);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Número");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
