/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.NuevaDireccion;
import controller.VerPerfilEmpleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Clase que maneja las direcciones
 * @author JONATHAN
 */
public class Direccion {

    private String provincia;
    private String canton;
    private String parroquia;
    private String descripcion;
    private String cedula;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    private Stage stagePadre;

    public Direccion(String cedula, String provincia, String canton, String parroquia, String descripcion) {
        this.cedula = cedula;
        this.provincia = provincia;
        this.canton = canton;
        this.parroquia = parroquia;
        this.descripcion = descripcion;
        conex = new Conexion();
    }

    public String getProvincia() {
        return provincia;
    }

    public void setStagePadre(Stage stagePadre) {
        this.stagePadre = stagePadre;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getCanton() {
        return canton;
    }

    public void setCanton(String canton) {
        this.canton = canton;
    }

    public String getParroquia() {
        return parroquia;
    }

    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void deleteDireccion() throws SQLException {
        if (util.Util.confirmationDialog("¿Seguro que quiere eliminar la dirección?")) {
            try {
                String query = "delete from direcciones where cedula=?";
                con = conex.conectarMySQL();
                ps = con.prepareStatement(query);
                ps.setString(1, cedula);
                if (ps.executeUpdate() > 0) {
                    util.Util.mostrarDialog("Se elimino la dirección con exito");
                    fabricarPerfilEmpleado(cedula);
                    if (this.stagePadre != null) {
                        this.stagePadre.close();
                    } else {
                        System.out.println("Padre null");
                    }
                } else {
                    util.Util.mostrarDialog("No se elimino la dirección con exito");
                }
            } catch (Exception e) {
                util.Util.mostrarDialogAlert(e.getMessage());
            } finally {
                if (con != null) {
                    con.close();
                }
            }
        }
    }

    private void fabricarPerfilEmpleado(String ci) {
        Empleado emp;
        boolean bandera = true;
        try {
            if (esGuardia(ci)) {
                emp = crearGuardia(ci);
            } else {
                emp = crearEmpleado(ci);
                bandera = false;
            }
            if (emp != null) {
                Stage stage = new Stage();
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilEmpleado.fxml"));
                Parent root = fxmlLoader.load();
                VerPerfilEmpleado pe = fxmlLoader.getController();
                pe.cargarDatos(emp, bandera, stage);
                Scene scene = new Scene(root);
                stage.setTitle("Datos del Empleado: " + emp.getNombres());
                stage.setScene(scene);
                stage.setResizable(false);
                stage.show();
            } else {
                System.out.println("Empleado null");
            }

        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private Boolean esGuardia(String ci) {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            return res.next();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    private LinkedList<String> cargarTelefonos(String ci) {
        LinkedList<String> telefonos = new LinkedList<>();
        Connection con2 = null;
        PreparedStatement ps2;
        ResultSet res2;
        try {
            con2 = conex.conectarMySQL();
            ps2 = con2.prepareStatement("select numero from telefonos where cedula=?");
            ps2.setString(1, ci);
            res2 = ps2.executeQuery();
            while (res2.next()) {
                telefonos.add(res2.getString("numero"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (con2 != null) {
                    con2.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return telefonos;
    }

    private Guardia crearGuardia(String ci) throws SQLException {
        Guardia g = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_perfil_guardia where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                g = new Guardia(res.getInt("cod_guardia"), res.getString("credencial"), res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
                g.setHorario(res.getString("horario"));
                g.setRuc(res.getString("RUC"));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return g;
    }

    private Empleado crearEmpleado(String ci) {
        Empleado emp = null;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from empleado where cedula=?");
            ps.setString(1, ci);
            res = ps.executeQuery();
            if (res.next()) {
                boolean genero = res.getString("genero").equals("M");
                boolean estado = res.getString("estado").equals("activo");
                emp = new Empleado(res.getString("nombres"), res.getString("apellidos"),
                        res.getString("cedula"), res.getString("estado_civil"), res.getDate("fecha_de_nacimiento").toLocalDate(),
                        res.getDate("fecha_ingreso").toLocalDate(), genero, estado, res.getString("email"), res.getString("seguro_social"), res.getString("codigo_depa"),
                        res.getString("cargo"), res.getString("foto"), cargarTelefonos(ci));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        return emp;
    }

    public boolean actualizarDireccion(String provincia, String canton, String parroquia, String descripcion) throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("update direcciones set provincia=?,canton=?,parroquia=?,Descripcion=? where cedula=?");
            ps.setString(1, provincia);
            ps.setString(2, canton);
            ps.setString(3, parroquia);
            ps.setString(4, descripcion);
            ps.setString(5, cedula);
            if (ps.executeUpdate() > 0) {
                util.Util.mostrarDialog("Se actualizo la dirección con exito");
                fabricarPerfilEmpleado(this.cedula);
                this.stagePadre.close();
                return true;
            } else {
                util.Util.mostrarDialogAlert("No se pudo actualizar la dirección");
            }

        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;

    }
    
    public void fabricarVistaNumeroUpdate() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevaDireccion.fxml"));
            Parent root = fxmlLoader.load();
            NuevaDireccion controlador = fxmlLoader.getController();
            controlador.cargarDatosActualizar(this,stage);
            Scene scene = new Scene(root);
            stage.setTitle("Actualizar Dirección");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    public boolean insertarDatos(Stage padre) throws SQLException{
        this.stagePadre=padre;
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into direcciones values(?,?,?,?,?)");
            ps.setString(1, this.cedula);
            ps.setString(2, this.provincia);
            ps.setString(3, this.canton);
            ps.setString(4, this.parroquia);
            ps.setString(5, this.descripcion);
            if(ps.executeUpdate() > 0){
                fabricarPerfilEmpleado(this.cedula);
                this.stagePadre.close();
                return true;
            }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }

}
