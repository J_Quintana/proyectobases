/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.Visor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class DocumentosPersonales {

    private String documento;
    private String id;
    private Empleado emp;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;

    public DocumentosPersonales(String documento, String id, Empleado emp) {
        this.documento = documento;
        this.id = id;
        this.emp = emp;
        conex = new Conexion();
    }

    public String getDocumento() {
        return documento;
    }

    public String getId() {
        return id;
    }

    public VBox dibujar() {
        VBox root = new VBox(10);
        root.setAlignment(Pos.TOP_CENTER);
        ImageView img = new ImageView(new Image("/resources/botones/factura.png"));
        img.setFitWidth(100);
        img.setFitHeight(100);
        Label lbid = new Label(this.id);
        HBox root2 = new HBox(10);
        root2.setAlignment(Pos.CENTER);
        root2.getChildren().addAll(btnDelete(), btnSee());
        root.getChildren().addAll(img, lbid, root2);
        return root;
    }

    public boolean eliminar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from doc_personales where id=? and cedula=?");
            ps.setString(1, id);
            ps.setString(2, this.emp.getCI());
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean insertar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into doc_personales(id,cedula,Documento) values(?,?,?)");
            ps.setString(1, id);
            ps.setString(2, this.emp.getCI());
            ps.setString(3, this.documento);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private ImageView btnDelete() {
        ImageView btn = null;
        try {
            btn = new ImageView(new Image("/resources/botones/delete.png"));
            if (Usuario.rol != 1) {
                btn.setVisible(false);
            }
            btn.setFitWidth(25);
            btn.setFitHeight(25);
            btn.setOnMousePressed(e -> {
                try {
                    if (util.Util.confirmationDialog("Seguro de que quiere eliminar")) {
                        if (eliminar()) {
                            util.Util.mostrarDialog("Se elimino con exito el documento");
                            this.emp.getPadre().close();
                            this.emp.fabricarPerfilEmpleado();
                        } else {
                            util.Util.mostrarDialog("No se pudo eliminar el documento");
                        }
                    }
                } catch (SQLException sqlex) {
                    util.Util.mostrarDialogAlert(sqlex.getMessage());
                }
            });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return btn;
    }

    private void generarVisor() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVisor.fxml"));
            Parent root = fxmlLoader.load();
            Visor pcl = fxmlLoader.getController();
            pcl.cargarDocumento(this);
            Scene scene = new Scene(root);
            stage.setTitle("Documento: " + this.id);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }

    }

    private ImageView btnSee() {
        ImageView btn = null;
        try {
            btn = new ImageView(new Image("/resources/botones/see.png"));
            btn.setFitWidth(25);
            btn.setFitHeight(25);
            btn.setOnMousePressed(e -> {
                this.generarVisor();
            });
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return btn;
    }

}
