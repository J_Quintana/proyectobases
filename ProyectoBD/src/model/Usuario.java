/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author JONATHAN
 */
public class Usuario {

    private String ci;
    private String foto;
    private String nombre;
    public static int rol;
    private String pass;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;

    public Usuario(String ci, String foto, String nombre, int rol, String pass) {
        this.ci = ci;
        this.foto = foto;
        this.nombre = nombre;
        this.rol = rol;
        this.pass = pass;
        conex = new Conexion();
    }

    public boolean insertDatos() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into usuarios values(?,?,?,?,?)");
            ps.setString(1, ci);
            ps.setString(2, nombre);
            ps.setInt(3, rol);
            ps.setString(4, pass);
            ps.setString(5, foto);
            int r = ps.executeUpdate();
            con.close();
            if(r>0)
                return true;
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public String getCi() {
        return ci;
    }

    public String getFoto() {
        return foto;
    }

    public String getNombre() {
        return nombre;
    }

    public int getRol() {
        return rol;
    }

    public String getPass() {
        return pass;
    }

}
