/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.VerPerfilEmpleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Empleado {

    private String nombre;
    private String apellido;
    private String CI;
    private String estadoCivil;
    private LocalDate nacimiento;
    private LocalDate ingreso;
    private boolean genero;//true hombre // false mujer
    private boolean estado;
    private String mail;
    private String seguro;
    private String codDepa;
    private String cargo;
    private String foto;
    private LinkedList<String> telefonos;//ya insertado
    //desde aqui no se ha insertado
    private Direccion direccion;
    private LinkedList<Vacaciones> vacaciones;
    private LinkedList<Referencia> referencias;
    private LinkedList<DocumentosPersonales> docPersonales;
    private LinkedList<Examenes> examenes;
    private LinkedList<Permisos> permisos;
    private LinkedList<Cursos> cursos;
    // fin de no insertados
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    private Stage padre;

    public Empleado(String nombre, String apellido, String CI, String estadoCivil, LocalDate nacimiento, LocalDate ingreso, boolean genero, boolean estado, String mail, String seguro, String codDepa, String cargo, String foto, LinkedList<String> telefonos) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.CI = CI;
        this.estadoCivil = estadoCivil;
        this.nacimiento = nacimiento;
        this.ingreso = ingreso;
        this.genero = genero;
        this.estado = estado;
        this.mail = mail;
        this.seguro = seguro;
        this.codDepa = codDepa;
        this.cargo = cargo;
        this.foto = foto;
        this.telefonos = telefonos;
        try {
            conex = new Conexion();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public boolean addVacacion(Vacaciones vacacion) {
        return this.vacaciones.add(vacacion);
    }

    public void setPadre(Stage padre) {
        this.padre = padre;
    }

    public boolean addReferencia(Referencia referencia) {
        return this.referencias.add(referencia);
    }

    public boolean addDocumento(DocumentosPersonales p) {
        return this.docPersonales.add(p);
    }

    public boolean addExamen(Examenes exa) {
        return this.examenes.add(exa);
    }

    public boolean addPermiso(Permisos permiso) {
        return this.permisos.add(permiso);
    }

    public boolean addCurso(Cursos curso) {
        return this.cursos.add(curso);
    }
    /**
     * metodo que inserta al empleado en la base de datos
     * @return
     * @throws SQLException 
     */
    public boolean insertDatos() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into empleado (cedula, nombres, apellidos, estado_civil, fecha_de_nacimiento, "
                    + "genero, email, seguro_social, estado, fecha_ingreso, codigo_depa, cargo, foto) values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, CI);
            ps.setString(2, nombre);
            ps.setString(3, apellido);
            ps.setString(4, estadoCivil);
            Date nac = Date.from(nacimiento.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(5, new java.sql.Date(nac.getTime()));
            if (genero) {
                ps.setString(6, "M");
            } else {
                ps.setString(6, "F");
            }
            ps.setString(7, mail);
            ps.setString(8, seguro);
            if (estado) {
                ps.setString(9, "activo");
            } else {
                ps.setString(9, "inactivo");
            }
            Date ing = Date.from(ingreso.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(10, new java.sql.Date(ing.getTime()));
            ps.setString(11, codDepa);
            ps.setString(12, cargo);
            ps.setString(13, foto);
            int r = ps.executeUpdate();
            con.close();
            if (r > 0) {
                return true;
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public String getCI() {
        return CI;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombre=" + nombre + ", apellido=" + apellido + ", CI=" + CI + ", estadoCivil=" + estadoCivil + ", nacimiento=" + nacimiento + ", ingreso=" + ingreso + ", genero=" + genero + ", estado=" + estado + ", mail=" + mail + ", seguro=" + seguro + ", codDepa=" + codDepa + ", cargo=" + cargo + ", foto=" + foto + '}';
    }

    public boolean inserTelefonos() throws SQLException {
        try {
            int cont = 0;
            int r = 0;
            con = conex.conectarMySQL();
            for (String s : this.telefonos) {
                ps = con.prepareStatement("insert into telefonos values(?,?)");
                ps.setString(1, s);
                ps.setString(2, this.CI);
                r = ps.executeUpdate();
                if (r > 0) {
                    cont++;
                }
            }
            con.close();
            if (r == telefonos.size()) {
                return true;
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    // get
    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public LocalDate getNacimiento() {
        return nacimiento;
    }

    public LocalDate getIngreso() {
        return ingreso;
    }

    public boolean isGenero() {
        return genero;
    }

    public boolean isEstado() {
        return estado;
    }

    public String getMail() {
        return mail;
    }

    public String getSeguro() {
        return seguro;
    }

    public String getCodDepa() {
        return codDepa;
    }

    public String getCargo() {
        return cargo;
    }

    public String getFoto() {
        return foto;
    }

    public LinkedList<String> getTelefonos() {
        return telefonos;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public LinkedList<Vacaciones> getVacaciones() {
        return vacaciones;
    }

    public LinkedList<Referencia> getReferencias() {
        return referencias;
    }

    public LinkedList<DocumentosPersonales> getDocPersonales() {
        return docPersonales;
    }

    public LinkedList<Examenes> getExamenes() {
        return examenes;
    }

    public LinkedList<Permisos> getPermisos() {
        return permisos;
    }

    public LinkedList<Cursos> getCursos() {
        return cursos;
    }

    public String getNombres() {
        return this.nombre + " " + this.apellido;
    }

    // otros metodos
    public boolean crearDireccion() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from direcciones where cedula=?");
            ps.setString(1, CI);
            res = ps.executeQuery();
            if (res.next()) {
                this.setDireccion(new Direccion(CI, res.getString("provincia"), res.getString("canton"),
                        res.getString("parroquia"), res.getString("Descripcion")));
                return true;
            }
        } catch (Exception ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public Stage getPadre() {
        return padre;
    }

    public boolean crearCursos() throws SQLException {
        this.cursos = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from cursos where cedula=?");
            ps.setString(1, CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.cursos.add(new Cursos(res.getString("tema"), res.getString("lugar"), res.getString("duracion"),
                        res.getInt("id_curso"), this));
            }
            return !this.cursos.isEmpty();
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean crearVacaciones() throws SQLException {
        this.vacaciones = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vacaciones where cedula=?");
            ps.setString(1, CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.addVacacion(new Vacaciones(res.getDate("fecha_inicio").toLocalDate(), res.getDate("fecha_fin").toLocalDate(),
                        this, res.getInt("id")));
            }
            return !this.vacaciones.isEmpty();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean cargarDocumentosPersonales() throws SQLException {
        this.docPersonales = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from doc_personales where cedula=?");
            ps.setString(1, CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.docPersonales.add(new DocumentosPersonales(res.getString("Documento"), res.getString("id"), this));
            }
            return !this.docPersonales.isEmpty();
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public void fabricarPerfilEmpleado() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilEmpleado.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilEmpleado pe = fxmlLoader.getController();
            this.setPadre(stage);
            pe.cargarDatos(this, false, stage);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Empleado: " + this.getNombres());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    public boolean cargarPermisos() throws Exception {
        this.permisos = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from permisos where cedula=?");
            ps.setString(1, CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.addPermiso(new Permisos(res.getString("tipo_de_permiso"), res.getString("motivo"), res.getDate("fecha_inicial").toLocalDate(),
                        res.getDate("fecha_final").toLocalDate(), res.getInt("id"), this, this.padre));
            }
            return !this.permisos.isEmpty();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }

        return false;
    }

    public boolean cargarExamenes() throws SQLException {
        this.examenes = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from examenes where cedula=?");
            ps.setString(1, this.CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.examenes.add(new Examenes(res.getInt("id"), this, res.getString("Resultado"),
                        res.getString("observacion"), res.getDate("fecha").toLocalDate()));
            }
            return !this.examenes.isEmpty();
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean cargarReferencias() {
        this.referencias = new LinkedList<>();
        try {
            crearReferenciasPersonal();
            crearReferenciasFamiliar();
            crearReferenciasLaboral();
            return !this.referencias.isEmpty();
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        return false;
    }

    private void crearReferenciasLaboral() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_referencia_laboral where cedula_empleado=?");
            ps.setString(1, this.CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.referencias.add(new ReferenciaLaboral(this, res.getString("cargo"), res.getString("empresa"),
                        res.getString("cedula"), res.getString("nombres"), res.getString("apellidos"), res.getString("email"),
                        "", res.getString("telefono")));
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    private void crearReferenciasFamiliar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_referencia_familiares where cedula_empleado=?");
            ps.setString(1, this.CI);
            res = ps.executeQuery();
            while (res.next()) {
                this.referencias.add(new CargaFamiliar(res.getDate("fecha_de_nacimiento").toLocalDate(), res.getString("ocupacion"),
                        res.getString("tipo_de_carga"),
                        res.getString("cedula"), res.getString("nombres"), res.getString("apellidos"), res.getString("email"),
                        res.getString("tipo"), res.getString("telefono"), this));
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    private void crearReferenciasPersonal() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from referencia where cedula_empleado=?");
            ps.setString(1, this.CI);
            res = ps.executeQuery();
            //Empleado e, String CI, String nombre, String apellidos, String email, String tipo, LinkedList<String> telefonos
            while (res.next()) {
                if (res.getString("tipo").equals("E")) {
                    this.referencias.add(new Referencia(this, res.getString("cedula"), res.getString("nombres"), res.getString("apellidos"),
                            res.getString("email"), res.getString("tipo"), res.getString("telefono")));
                }
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public boolean update_desactivar(boolean bandera) throws SQLException {
        //bandera true desactiva false activa
        try {
            if (bandera) {
                con = conex.conectarMySQL();
                ps = con.prepareStatement("update empleado set estado='inactivo' where cedula=?");
                ps.setString(1, this.CI);
                if (ps.executeUpdate() > 0) {
                    return true;
                }

            } else {
                con = conex.conectarMySQL();
                ps = con.prepareStatement("update empleado set estado='activo' where cedula=?");
                ps.setString(1, this.CI);
                if (ps.executeUpdate() > 0) {
                    return true;
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean eliminar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from empleado where cedula=?");
            ps.setString(1, this.CI);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

}
