/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.NuevoCurso;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Cursos {

    private String tema;
    private String lugar;
    private String duracion;
    private Empleado e;
    private Connection con;
    private PreparedStatement ps;
    private final Conexion conex;
    private int id;

    public Cursos(String tema, String lugar, String duracion, int id, Empleado e) {
        this.tema = tema;
        this.lugar = lugar;
        this.duracion = duracion;
        this.e = e;
        conex = new Conexion();
        this.id = id;
    }

    public String getTema() {
        return tema;
    }

    public String getLugar() {
        return lugar;
    }

    public String getDuracion() {
        return duracion;
    }

    public int getId() {
        return id;
    }

    public HBox dibujar() {
        VBox root = new VBox(5);
        HBox root2 = new HBox(5);
        root2.setAlignment(Pos.CENTER);
        Label lbtema = new Label("Tema: " + this.tema);
        Label lblugar = new Label("Lugar: " + this.lugar);
        Label lbduracion = new Label("Duración: " + this.duracion);
        root.getChildren().addAll(lbtema, lblugar, lbduracion);
        root2.getChildren().addAll(root, btnUpdate(), btnDelete());
        return root2;
    }

    private ImageView btnDelete() {
        ImageView btn = new ImageView(new Image("/resources/botones/delete.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            if (util.Util.confirmationDialog("Seguro que quiere eliminar la vacacion")) {
                try {
                    if(eliminar()){
                        util.Util.mostrarDialog("Se elimino con exito el curso");
                        this.e.getPadre().close();
                        this.e.fabricarPerfilEmpleado();
                    }
                    else {
                        util.Util.mostrarDialog("No se elimino el curso");
                    }
                } catch (SQLException ex) {
                    util.Util.mostrarDialogAlert(ex.getMessage());
                }
            }
        });
        return btn;
    }

    private ImageView btnUpdate() {
        ImageView btn = new ImageView(new Image("/resources/botones/update.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            crearVistaUpdate();
        });
        return btn;
    }

    public boolean insertar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into cursos (id_curso,tema,lugar,duracion,cedula) values(?,?,?,?,?)");
            ps.setInt(1, id);
            ps.setString(2, tema);
            ps.setString(3, lugar);
            ps.setString(4, duracion);
            ps.setString(5, this.e.getCI());
            if (ps.executeUpdate() > 0) {
                return true;
            }

        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public boolean actualizar(String tema, String lugar, String duracion, int id) throws SQLException {
        this.tema = tema;
        this.lugar = lugar;
        this.duracion = duracion;
        
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("update cursos set id_curso=?, tema=?, lugar=?, duracion=? where id_curso=? and cedula=?");
            ps.setInt(1, id);
            ps.setString(2, this.tema);
            ps.setString(3, this.lugar);
            ps.setString(4, this.duracion);
            ps.setInt(5, this.id);
            ps.setString(6, this.e.getCI());
            if(ps.executeUpdate() > 0)
                return true;
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }
    
    private void crearVistaUpdate(){
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoCurso.fxml"));
            Parent root = fxmlLoader.load();
            NuevoCurso controlador = fxmlLoader.getController();
            controlador.cargarDatos(this.e, false,stage, this.e.getPadre());
            controlador.cargarUpdate(this);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Permiso");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public boolean eliminar() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from cursos where id_curso=? and cedula=?");
            ps.setInt(1, id);
            ps.setString(2, this.e.getCI());
            if(ps.executeUpdate() > 0)
                return true;
        }
        catch(SQLException ex){
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }

}
