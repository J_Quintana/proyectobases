/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.DatosReferencia;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class ReferenciaLaboral extends Referencia{
    private String cargo;
    private String empresa;
    private Conexion conex;
    private Connection con;
    private PreparedStatement ps;

    public ReferenciaLaboral(Empleado e, String cargo, String empresa, String CI, String nombre, String apellidos, String email, String tipo,String telefonos) {
        super(e,CI, nombre, apellidos, email, tipo, telefonos);
        this.cargo = cargo;
        this.empresa = empresa;
        conex = new Conexion();
    }

    
    public boolean insert() throws SQLException{
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into laboral values (?,?,?)");
            ps.setString(1, super.getCI());
            ps.setString(2, cargo);
            ps.setString(3, empresa);
            if(super.insertar()){
                int r = ps.executeUpdate();
                con.close();
                if(r>0)
                    return true;
            }
        }
        catch(Exception e){
            util.Util.mostrarDialogAlert(e.getMessage());
        }
        finally {
            if(con != null)
                con.close();
        }
        return false;
    }

    public String getCargo() {
        return cargo;
    }

    public String getEmpresa() {
        return empresa;
    }
    
    @Override
    public HBox dibujar() {
        String cssLayout = "-fx-border-color: red;\n"
                + "-fx-border-width: 1;\n"
                + "-fx-border-style: dashed;\n";
        HBox root = new HBox(5);
        VBox root2 = new VBox(3);
        Label lbCedula = new Label("Cedula: " + this.getCI());
        Label lbNombre = new Label(this.getNombre() + " " + this.getApellidos());
        String tmp;
        if (this.getTipo().equals("P")) {
            tmp = "Personal";
        } else if (this.getTipo().equals("E")) {
            tmp = "Emergencia";
        } else {
            tmp = "Laboral";
        }
        Label lbTipo = new Label("Tipo: " + tmp);
        root2.getChildren().addAll(lbCedula, lbNombre, lbTipo);
        root.getChildren().addAll(root2, btnSee(),btnDelete());
        root.setStyle(cssLayout);
        root.setAlignment(Pos.CENTER);
        return root;
    }

    private ImageView btnSee() {
        ImageView btn = new ImageView(new Image("/resources/botones/see.png"));
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            this.fabricarVista();
        });
        return btn;
    }
    
    private void fabricarVista(){
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLDatosReferencia.fxml"));
            Parent root = fxmlLoader.load();
            DatosReferencia controller = fxmlLoader.getController();
            controller.cargarReferencia(this);
            Scene scene = new Scene(root);
            stage.setTitle("Datos de Referencia ");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }
    }

    private ImageView btnDelete() {
        ImageView btn = new ImageView(new Image("/resources/botones/delete.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMouseClicked(e -> {
            System.out.println("Eliminando");
            try {
                if(util.Util.confirmationDialog("Seguro que quiere eliminar el registro")){
                    if(this.eliminar()){
                        this.getEmpleado().fabricarPerfilEmpleado();
                        this.getEmpleado().getPadre().close();
                    }
                }
            }
            catch(SQLException ex){
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        });
        return btn;
    }
    
}
