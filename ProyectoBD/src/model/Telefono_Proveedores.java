/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.NuevoTelefono;
import controller.VerPerfilProveedor;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author CEEMP02
 */
public class Telefono_Proveedores {

    private final String numero;
    private final Proveedores proveedor;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private final Stage stage;

    public Telefono_Proveedores(String numero, Proveedores objeto, Stage stage) {
        this.numero = numero;
        this.proveedor = objeto;
        this.stage = stage;
        conex = new Conexion();
    }

    private boolean deleteTelefono() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement(proveedor.queryDeleteTelefono());
            ps.setString(1, this.proveedor.getRuc());
            ps.setString(2, numero);
            int r = ps.executeUpdate();
            if (r > 0) {
                return true;
            }

        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private void fabricarPerfilProveedor(Proveedores prov) {
        try {
            Stage stage = new Stage();
            prov.setStage(stage);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilProveedor.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilProveedor pcl = fxmlLoader.getController();
            pcl.cargarDatosProveedor(prov);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Proveedor: " + prov.getNombre());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (IOException i) {
            Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Failed to create new Window.", i);
        }
    }

    private void fabricarVistaNumeroUpdate() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLNuevoTelefono.fxml"));
            Parent root = fxmlLoader.load();
            NuevoTelefono controlador = fxmlLoader.getController();
            String query ="update telefono_proveedor set telefono=? where telefono='"+this.numero+"' and ruc=?";
            controlador.cargarDatos(numero, query, this.proveedor.getRuc());
            controlador.setStage(stage);
            controlador.setBandera(true);
            controlador.setObjeto(this.proveedor);
            controlador.setStagePadre(this.stage);
            Scene scene = new Scene(root);
            stage.setTitle("Nuevo Número");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    public HBox dibujarNumero() {
        Label lbNumero = new Label(this.numero);
        final ImageView btnUpdate = new ImageView(new Image("/resources/botones/update.png"));
        final ImageView btnDelete = new ImageView(new Image("/resources/botones/delete.png"));
        btnUpdate.setFitWidth(25);
        btnUpdate.setFitHeight(25);
        btnDelete.setFitWidth(25);
        btnDelete.setFitHeight(25);
        HBox root = new HBox(10);
        root.getChildren().addAll(lbNumero, btnDelete, btnUpdate);
        btnDelete.setOnMousePressed(e -> {
            try {
                if (this.deleteTelefono()) {
                    util.Util.mostrarDialog("Se elimino con exito el número!!");
                    this.fabricarPerfilProveedor(this.proveedor);
                    if (this.stage != null) {
                        this.stage.close();
                    } else {
                        System.out.println("Soy nulo");
                    }
                } else {
                    util.Util.mostrarDialogAlert("No se pudo eliminar el numero");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Telefono_Proveedores.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        btnUpdate.setOnMousePressed(e -> {
            this.fabricarVistaNumeroUpdate();
        });
        root.setAlignment(Pos.CENTER);
        return root;
    }
    
    

}
