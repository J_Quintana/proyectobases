/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JONATHAN
 */
public class Permisos {

    private String tipo;
    private String motivo;
    private LocalDate inicio;
    private LocalDate fin;
    private int id;
    private Empleado e;
    private Connection con;
    private PreparedStatement ps;
    private final Conexion conex;
    private Stage padre;

    public Permisos(String tipo, String motivo, LocalDate inicio, LocalDate fin, int id, Empleado e,Stage padre) {
        this.tipo = tipo;
        this.motivo = motivo;
        this.inicio = inicio;
        this.fin = fin;
        this.id = id;
        this.e = e;
        this.padre = padre;
        conex = new Conexion();
    }

    public HBox dibujar() {
        VBox root = new VBox(5);
        HBox rootP = new HBox(5);
        rootP.setAlignment(Pos.CENTER_LEFT);
        root.setAlignment(Pos.CENTER_LEFT);
        Label lbid = new Label("ID: " + this.id);
        Label lbTipo = new Label("Tipo: " + this.tipo);
        Label lbMotivo = new Label("Motivo: " + this.motivo);
        Label lbInicio = new Label("Inicio: " + this.inicio.toString());
        Label lbFin = new Label("Fin: " + this.fin.toString());
        root.getChildren().addAll(lbid, lbTipo, lbMotivo, lbInicio, lbFin);
        rootP.getChildren().addAll(root, btnDelete());
        return rootP;
    }

    private ImageView btnDelete() {
        ImageView btn = new ImageView(new Image("/resources/botones/delete.png"));
        if(Usuario.rol != 1){
            btn.setVisible(false);
        }
        btn.setFitWidth(25);
        btn.setFitHeight(25);
        btn.setOnMousePressed(e -> {
            try {
                if (util.Util.confirmationDialog("Seguro que desea eliminar el permiso?")) {
                    if (this.eliminar()) {
                        util.Util.mostrarDialog("Se elimino con exito");
                        this.e.fabricarPerfilEmpleado();
                        this.padre.close();
                    } else {
                        util.Util.mostrarDialog("No se pudo eliminar el permiso");
                    }
                }
            }
            catch(SQLException ex){
                util.Util.mostrarDialogAlert(ex.getMessage());
            }
        });
        return btn;
    }

    public boolean insertarDatos(String cedula) throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into permisos (tipo_de_permiso,motivo,fecha_inicial,fecha_final,cedula) values(?,?,?,?,?)");
            ps.setString(1, tipo);
            ps.setString(2, motivo);
            java.util.Date dat = java.util.Date.from(this.inicio.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(3, new Date(dat.getTime()));
            java.util.Date dat2 = java.util.Date.from(this.fin.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            ps.setDate(4, new Date(dat2.getTime()));
            ps.setString(5, cedula);
            if (ps.executeUpdate() > 0) {
                return true;
            }

        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    private boolean eliminar() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("delete from permisos where id=?");
            ps.setInt(1, id);
            if (ps.executeUpdate() > 0) {
                return true;
            }
        } catch (SQLException ex) {
            util.Util.mostrarDialogAlert(ex.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

}
