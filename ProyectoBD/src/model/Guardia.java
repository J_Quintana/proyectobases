/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Conexion;
import controller.VerPerfilEmpleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.LinkedList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *Clase que maneja al guardia
 * @author JONATHAN
 */
public class Guardia extends Empleado {

    private final int codGuardia;
    private final String credencial;
    private String ruc;
    private LinkedList<PruebasFisicas> pruebas;
    private String horario;
    private final Conexion conex;
    private Connection con;
    private PreparedStatement ps;
    private ResultSet res;
    private LinkedList<Insumos> insumos;
    private Stage ventanaInsumos;

    public Guardia(int codGuardia, String credencial, String nombre, String apellido, String CI, String estadoCivil, LocalDate nacimiento, LocalDate ingreso, boolean genero, boolean estado, String mail, String seguro, String codDepa, String cargo, String foto, LinkedList<String> telefonos) {
        super(nombre, apellido, CI, estadoCivil, nacimiento, ingreso, genero, estado, mail, seguro, codDepa, cargo, foto, telefonos);
        this.codGuardia = codGuardia;
        this.credencial = credencial;
        this.conex = new Conexion();
    }

    public Stage getVentanaInsumos() {
        return ventanaInsumos;
    }

    public void setVentanaInsumos(Stage ventanaInsumos) {
        this.ventanaInsumos = ventanaInsumos;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public int getCodGuardia() {
        return codGuardia;
    }

    public LinkedList<Insumos> getInsumos() {
        return insumos;
    }

    public String getCredencial() {
        return credencial;
    }

    public String getRuc() {
        return ruc;
    }

    public LinkedList<PruebasFisicas> getPruebas() {
        return pruebas;
    }

    public String getHorario() {
        return horario;
    }

    public boolean addPrueba(PruebasFisicas prueba) {
        return this.pruebas.add(prueba);
    }

    @Override
    public boolean insertDatos() throws SQLException {
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("insert into guardia (cod_guardia, cedula,credencial) values(?,?,?)");
            super.insertDatos();
            ps.setInt(1, codGuardia);
            ps.setString(2, super.getCI());
            ps.setString(3, credencial);
            int r = ps.executeUpdate();
            con.close();
            if (r > 0) {
                return true;
            }
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return false;
    }

    public void crearPruebasFisicas() throws SQLException {
        this.pruebas = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from pruebas_fisicas where cod_guardia=?");
            ps.setInt(1, this.codGuardia);
            res = ps.executeQuery();
            while (res.next()) {
                this.addPrueba(new PruebasFisicas(res.getInt("id"), res.getString("nombre"), res.getString("resultado"),this));
            }
        } catch (SQLException e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    @Override
    public void fabricarPerfilEmpleado() {
        try {
            Stage stage = new Stage();
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/FXMLVerPerfilEmpleado.fxml"));
            Parent root = fxmlLoader.load();
            VerPerfilEmpleado pe = fxmlLoader.getController();
            pe.cargarDatos(this, true, stage);
            Scene scene = new Scene(root);
            stage.setTitle("Datos del Empleado: " + this.getNombres());
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
        } catch (Exception e) {
            util.Util.mostrarDialogAlert(e.getMessage());
        }

    }
    
    public boolean crearInsumos() throws SQLException{
        this.insumos = new LinkedList<>();
        try {
            con = conex.conectarMySQL();
            ps = con.prepareStatement("select * from vista_insumos_guardia where cod_guardia=?");
            ps.setInt(1,codGuardia);
            res = ps.executeQuery();
            while(res.next()){
                this.insumos.add(new Insumos(res.getString("nombre"),res.getString("descripcion"),res.getInt("id"),this));
            }
            return !this.insumos.isEmpty();
        }
        catch(SQLException ex){
            util.Util.mostrarDialogAlert(ex.getMessage());
        }
        finally{
            if(con != null)
                con.close();
        }
        return false;
    }
    
}
