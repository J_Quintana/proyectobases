CREATE DATABASE GRSECURE;
USE GRSECURE;
/*rol 1 para gerente 2 contadora y 3 para asistente*/
create table usuarios (
cedula varchar(10) primary key,
nombre varchar(100) not null,
rol char(1) not null,
pass varchar(50) not null,
foto LONGBLOB not null
);

CREATE TABLE Departamento (
ID VARCHAR (50) PRIMARY KEY,
Sueldo DOUBLE NOT NULL,
Descripcion VARCHAR (50)
);

CREATE TABLE Cliente(  
ruc VARCHAR (13) PRIMARY KEY,
nombre_empresa VARCHAR (150) NOT NULL,
direccion VARCHAR (200) NOT NULL,
telefono VARCHAR (10) NOT NULL,
actividad_principal VARCHAR (50),
representante_legal VARCHAR (100) NOT NULL
);
CREATE TABLE Proveedores(
ruc VARCHAR (13) PRIMARY KEY,
nombre VARCHAR (100) NOT NULL,
direccion VARCHAR (150) NOT NULL
);
CREATE TABLE Empleado(
cedula VARCHAR (10) PRIMARY KEY NOT NULL,
nombres VARCHAR (150) NOT NULL,
apellidos VARCHAR (150) NOT NULL,
estado_civil VARCHAR (20),
fecha_de_nacimiento DATE NOT NULL,
genero CHAR (1) CHECK (genero=’F’ or genero=’M’),
email VARCHAR (150),
seguro_social VARCHAR (10),
estado VARCHAR(8) CHECK (estado=’activo’ or estado=’inactivo’),
fecha_ingreso DATE NOT NULL,
codigo_depa VARCHAR(50) NOT NULL,
cargo VARCHAR(50) NOT NULL,
foto LONGBLOB not null,
FOREIGN KEY (codigo_depa) REFERENCES Departamento(ID)
);
CREATE TABLE Direcciones(
cedula VARCHAR (10) PRIMARY KEY,
provincia VARCHAR (50) NOT NULL,
canton VARCHAR (50) NOT NULL,
parroquia VARCHAR (50) NOT NULL,
Descripcion VARCHAR (200) NOT NULL,
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);

CREATE TABLE Vacaciones(
id int auto_increment,
fecha_inicio DATE NOT NULL,
fecha_fin DATE NOT NULL,
cedula VARCHAR (10),
PRIMARY KEY (id, cedula),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);
CREATE TABLE Telefonos(
numero VARCHAR (10),
cedula VARCHAR (10),
PRIMARY KEY (numero, cedula),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);
CREATE TABLE Referencia(
cedula VARCHAR (10) PRIMARY KEY,
nombres VARCHAR (150) NOT NULL,
apellidos VARCHAR (150) NOT NULL,
email VARCHAR (150),
tipo CHAR (1) CHECK (tipo=‘P’ or tipo=‘E’),
cedula_empleado VARCHAR (10) NOT NULL,
telefono varchar(10) not null,
FOREIGN KEY (cedula_empleado) REFERENCES Empleado(cedula)
 );
CREATE TABLE Cargas_Familiares(  
cedula VARCHAR(10) PRIMARY KEY,
fecha_de_nacimiento DATE NOT NULL,
ocupacion VARCHAR(50),
tipo_de_carga CHAR(1) NOT NULL,
FOREIGN KEY (cedula) REFERENCES Referencia(cedula),
CONSTRAINT c1 CHECK (tipo_de_carga=‘C’ or tipo_de_carga=‘H’) 
);
CREATE TABLE Laboral( 
cedula VARCHAR (10) PRIMARY KEY,
cargo VARCHAR(100),
empresa VARCHAR(150) NOT NULL,
FOREIGN KEY (cedula) REFERENCES Referencia(cedula) );

CREATE TABLE Guardia(
cod_guardia INT PRIMARY KEY,
credencial LONGBLOB not null,
horario VARCHAR(50), 
cedula VARCHAR (10),
RUC VARCHAR (13),
FOREIGN KEY(RUC) REFERENCES cliente(RUC),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);

CREATE TABLE Pruebas_fisicas(
id INT auto_increment PRIMARY KEY,
nombre VARCHAR (100) NOT NULL,
resultado VARCHAR (200) NOT NULL,
cod_guardia INT,
FOREIGN KEY (cod_guardia) REFERENCES Guardia(cod_guardia)
);

CREATE TABLE Doc_personales(
id varchar(20),
cedula VARCHAR (10),
Documento LONGBLOB not null,
PRIMARY KEY (id, cedula),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);



CREATE TABLE Examenes(
Id int auto_increment PRIMARY KEY,
Resultado LONGBLOB not null,
observacion VARCHAR (300),
fecha DATE NOT NULL,
cedula VARCHAR (10),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);

CREATE TABLE Permisos(
id INT auto_increment PRIMARY KEY,
tipo_de_permiso VARCHAR (10) NOT NULL,
motivo VARCHAR (50) NOT NULL,
fecha_inicial DATE NOT NULL,
fecha_final DATE NOT NULL,
cedula VARCHAR (10),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);

CREATE TABLE Cursos(
id_curso INT,
tema VARCHAR (200) NOT NULL,
lugar VARCHAR (50) NOT NULL,
duracion VARCHAR (10) NOT NULL,
cedula VARCHAR (10),
primary key(id_curso,cedula),
FOREIGN KEY (cedula) REFERENCES Empleado(cedula)
);

CREATE TABLE Facturas(
Id INT auto_increment,
documento LONGBLOB not null,
fecha DATE,
ruc VARCHAR (13),
PRIMARY KEY (Id, ruc),
FOREIGN KEY (ruc) REFERENCES Proveedores(ruc)
);
CREATE TABLE Productos(
id INT auto_increment PRIMARY KEY,
nombre VARCHAR (100) NOT NULL,
descripcion VARCHAR (200) NOT NULL,
ruc VARCHAR (13),
FOREIGN KEY (ruc) REFERENCES Proveedores(ruc)
);
CREATE TABLE Telefono_proveedor(
telefono VARCHAR (10),
ruc VARCHAR (13),
PRIMARY KEY (telefono, ruc),
FOREIGN KEY (ruc) REFERENCES Proveedores(ruc)
);
CREATE TABLE Insumos(
id INT auto_increment PRIMARY KEY,
cod_guardia INT,
id_producto INT,
FOREIGN KEY (cod_guardia) REFERENCES Guardia(cod_guardia),
FOREIGN KEY (id_producto) REFERENCES Productos(id)
);
